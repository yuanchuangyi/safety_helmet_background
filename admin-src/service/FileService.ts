
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';


import { File } from '../../src/models/File';

@Injectable()
export class FileService {

  private baseUrl =  'api/fileupload';

  constructor(
    private http: HttpClient
  ) {
  }

  getFiles(): Observable<File[]> {
    return this.http.get<File[]>(this.baseUrl);
  }

  getFile(id: string): Observable<File> {
    return this.http.get<File>(`${this.baseUrl}/${id}`);
  }

  del(id: number): Observable<any> {
    const url = `${this.baseUrl}/${id}`;
    return this.http.delete(url);
  }

  delAllByIds(ids: string[]): Observable<any> {
    const url = `${this.baseUrl}/delete`;
    let params: HttpParams = new HttpParams().append('where', JSON.stringify({id: {$in: ids}}));
    return this.http.delete(url, { params: params});
  }

  delEditorFileBySrc(src: string): Observable<any> {
    const url = `api/editor${src.substring(src.indexOf('/'))}`;
    return this.http.delete(url);
  }

}
