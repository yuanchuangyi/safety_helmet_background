
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import {map, switchMapTo} from 'rxjs/operators';
import { environment } from '../environments/environment';
import { WorkMsg } from '../models/WorkMsg';

@Injectable()
export class WorkmsgService {
  private baseUrl = environment.apiUrl + 'api/workprojectmsg';
  constructor(
    private http: HttpClient
  ) {
  }
  getallmsg( current: number = 0, pageSize: number = environment.pageSize, options: {  projectId?: number }): Observable<{res: WorkMsg[], success: boolean, total: number}> {
    const paramsObj: any = {
      pageSize: String(pageSize),
      page: String(current)
    };
    if (options.projectId) paramsObj.projectId = options.projectId;
    const params = new HttpParams({ fromObject: paramsObj });
    return this.http.get<{res: WorkMsg[], success: boolean, total: number}>(`${this.baseUrl}/list`,{params: params});
  }

  deleteMsg(id): Observable<{res: WorkMsg, success: boolean}> {
    return this.http.delete<{res: WorkMsg, success: boolean}>(`${this.baseUrl}/del/${id}`);
  }

  insertMsg(workmsg: WorkMsg): Observable<{res: WorkMsg, success: boolean}> {
    return this.http.post<{res: WorkMsg, success: boolean}>(`${this.baseUrl}/insert`, workmsg);
  }
}
