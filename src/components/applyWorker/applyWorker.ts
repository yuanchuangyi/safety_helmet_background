import { Component, OnInit, ViewChild, Inject, } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {MatDialogRef, MAT_DIALOG_DATA, MatDatepickerInputEvent, MatDialog} from '@angular/material';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { WorkProject } from '../../models/WorkProject';
import { Workjob } from '../../models/Workjob';
import { JobUser, StatusType, statusType, TypeNameMap } from '../../models/JobUser';

import { WorkjobService } from '../../service/WorkjobService';
import { WorkplaceService } from '../../service/WorkplaceService';
import { JobuserService } from '../../service/jobuserService';

import {ConfirmDialogComponent} from '../confirmDialog/confirmDialog';


@Component({
  templateUrl: './applyWorker.html',
  styleUrls: ['./applyWorker.css']
})
export class ApplyWorkerComponent implements OnInit {

  selection: SelectionModel<Workjob>;
  workjob: Workjob;
  workers: JobUser[];
  total: number;
  subscriptions: Subscription[] = [];
  pageSize = environment.pageSize;
  page = 0;
  workplace: WorkProject;
  projectId: number;
  jobId: number;
  status: number = 0;
  statusType = statusType;
  TypeNameMap = TypeNameMap;
  haveWorker: boolean = true;
  cols: string[] = ['select', 'username', 'sex', 'address', 'phone', 'status'];
  constructor(
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<ApplyWorkerComponent>,
    private route: ActivatedRoute,
    private router: Router,
    private workjobService: WorkjobService,
    private workplaceService: WorkplaceService,
    private jobuserService: JobuserService,
    @Inject(MAT_DIALOG_DATA) public data: { workjob: Workjob, projectid: number }
  ) {
    this.workjob = data.workjob;
    this.projectId = data.projectid;
  }

  ngOnInit() {
    this.cols = ['select', 'username', 'sex', 'address', 'phone', 'status', 'operate'];

    const initialSelection = [];
    const allowMultiSelect = true;
    this.selection = new SelectionModel<JobUser>(allowMultiSelect, initialSelection);
    this.jobId = this.workjob.id;
    this.update(this.page, this.jobId, this.projectId, this.status);
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.workers.length;
    return numSelected === numRows;
  }

  allToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.workers.forEach(row => this.selection.select(row));
  }


  goInterview(id) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      height: '200px',
      data: {message: '确认通知此工人来面试吗？'}
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.jobuserService.updateStatus(id, 2).subscribe((res) => {
          if (res.success) {
            this.update(this.page, this.jobId, this.projectId, 2);
          }
        })
      }
    });
  }

  goEnter(id) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      height: '200px',
      data: {message: '确认通知此工人来入职吗？'}
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.jobuserService.updateStatus(id, 3).subscribe((res) => {
          if (res.success) {
            this.update(this.page, this.jobId, this.projectId, 3);
          }
        })
      }
    });
  }

  onReject(id) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      height: '200px',
      data: {message: '确认拒绝此工人吗？'}
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.jobuserService.updateStatus(id, 4).subscribe((res) => {
          if (res.success) {
            this.update( this.page, this.jobId, this.projectId, 4);
          }
        })
      }
    });
  }

  update(page, jobId, projectId, status) {
    this.jobuserService.getjobuser(page, this.pageSize, {jobId, projectId, status}).subscribe((res) => {
      this.workers = res.res;
      this.total = res.total;
      if (this.total === 0) {
        this.haveWorker = false;
      }
    })
  }

  onPageChanged(page: number) {
      this.update(page, this.jobId, this.projectId, this.status);
  }

  trackById(index: number, item: any) {
    return item.id;
  }

}
