import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';


import { environment } from '../../environments/environment';
import { ConfirmDialogComponent } from '../confirmDialog/confirmDialog';
import { WorkplaceAddComponent} from '../workplaceAdd/workplaceAdd';
import { PostJobComponent } from '../postJob/postJob';
import { WorkJobComponent } from '../workJob/workJob';
import { WorkmsgComponent } from '../workmsg/workmsg';
import { WorkplaceMapComponent } from '../workplaceMap/workplaceMap';

import { WorkProject } from '../../models/WorkProject';
import {User} from '../../models/User';
import { WorkplaceService } from '../../service/WorkplaceService';
import { CompanyService } from '../../service/CompanyService';
import { Company } from '../../models/Company';
import { CheckStatus,  CheckStatusMap } from '../../models/Company';
import {StatusType} from '../../models/JobUser';
declare var BMap: any;
@Component({
  templateUrl: './workplace.html',
  styleUrls: ['./workplace.css']
})
export class WorkplaceComponent implements OnInit {

  selection: SelectionModel<WorkProject>;
  workPlaces: WorkProject[];
  total: number;
  subscriptions: Subscription[] = [];
  pageSize = environment.pageSize;
  page = 0;
  company: Company;
  longitude: string;
  latitude: string;
  addressname: string;
  img: string;
  imgAppear: boolean = false;
  status = CheckStatus.UNREVIEW;
  CheckStatus = CheckStatus;
  CheckStatusMap = CheckStatusMap;
  cols: string[] = ['select', 'projectCode', 'projectName', 'description', 'projectAddress', 'projectImg', 'projectPhone', 'startDate', 'endDate', 'totalWorkers', 'nowWorkers', 'status'];
  constructor(
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private router: Router,
    private workplaceService: WorkplaceService,
    private companyService: CompanyService,

  ) { }

  ngOnInit() {
    this.cols = ['select', 'projectCode', 'projectName', 'description', 'projectAddress', 'projectImg', 'projectPhone', 'startDate', 'endDate', 'totalWorkers', 'nowWorkers', 'status', 'operate'];
    this.subscriptions[1] = this.route.data
      .subscribe((data: { results: { workplaces: WorkProject[], total: number}}) => {
        this.workPlaces = data.results.workplaces;
        this.total = data.results.total;
      });
    const initialSelection = [];
    const allowMultiSelect = true;
    this.selection = new SelectionModel<WorkProject>(allowMultiSelect, initialSelection);
    this.companyService.getCompany().subscribe(res => {
      this.company = res.res[0];
    })
  }

  onStatusChange(status: CheckStatus) {
    this.status = status;
    this.update({pageIndex: this.page, pageSize: this.pageSize});
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.workPlaces.length;
    return numSelected === numRows;
  }

  allToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.workPlaces.forEach(row => this.selection.select(row));
  }


  onDelete(id) {
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        width: '400px',
        height: '200px',
        data: {message: '确认删除？'}
      });
      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          this.workplaceService.deleteProject(id).subscribe((res) => {
            if (res.success) {
              this.update({pageIndex: 0, pageSize: this.pageSize});
            }
          });
        }
      });
  }
  onOpenAdd() {
      const dialogRef = this.dialog.open(WorkplaceAddComponent, {
        width: '500px',
        height: '700px',
        data: {
          company: this.company
        }
      });
      dialogRef.afterClosed().subscribe((result) => {
        this.update({pageIndex: this.page, pageSize: this.pageSize});
      });
  }

  onOpenEdit(workplace) {
      const dialogRef = this.dialog.open(WorkplaceAddComponent, {
        width: '500px',
        height: '700px',
        data: {
          workplace: workplace,
          company: this.company
        }
      });
      dialogRef.afterClosed().subscribe((result) => {
        this.update({pageIndex: this.page, pageSize: this.pageSize});
      });
  }

  onPostjob(workplace) {
    const dialogRef = this.dialog.open( PostJobComponent, {
      width: '500px',
      height: '600px',
      data: {
        workplace: workplace,
        company: this.company
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.update({pageIndex: this.page, pageSize: this.pageSize});
    });
  }

  onFindjob(workplace) {
    const dialogRef = this.dialog.open( WorkJobComponent, {
      width: '800px',
      height: '400px',
      data: {
        workplace: workplace
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.update({pageIndex: this.page, pageSize: this.pageSize});
    });
  }

  onOpenMsg(workplace) {
    const dialogRef = this.dialog.open( WorkmsgComponent, {
      width: '800px',
      height: '400px',
      data: {
        workplace: workplace
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.update({pageIndex: this.page, pageSize: this.pageSize});
    });
  }

  onOpenDetail(workplace) {
    const dialogRef = this.dialog.open( WorkplaceMapComponent, {
      width: '800px',
      height: '400px',
      data: {
        workplace: workplace
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.update({pageIndex: this.page, pageSize: this.pageSize});
    });
  }

  update(event: {pageIndex: number, pageSize: number}) {
    this.workplaceService.getmylist(event.pageIndex, event.pageSize, this.status).subscribe((res) => {
      this.workPlaces = res.res;
      this.total = res.total;
      this.page = event.pageIndex;
    });
  }

  trackById(index: number, item: any) {
    return item.id;
  }
  lookBigimg(img) {
    this.img = img;
    this.imgAppear = true;
  }
  closeimg(){
    this.imgAppear = false;
  }
}
