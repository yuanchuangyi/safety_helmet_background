import { Component, OnInit } from '@angular/core';

import { WorkMsg } from '../../../src/models/WorkMsg';
import { environment } from '../../../src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';

import { ConfirmDialogComponent } from '../../../src/components/confirmDialog/confirmDialog';
import { MatDialog, MatSnackBar } from '@angular/material';
import { MessageService } from '../../service/MessageService';
import {CheckStatus} from '../../../src/models/Company';
import {tap} from 'rxjs/operators';
import {adjustList, trackById} from '../../../src/others/utils';

@Component({
  templateUrl: './messageManage.html',
  styleUrls: ['./messageManage.css']
})
export class MessageManageComponent implements OnInit {
  status: CheckStatus;
  workMsgs: WorkMsg[];
  workMsg: WorkMsg;
  total: number;
  currentPage = 1;
  pageSize = environment.adminPageSize;
  environment = environment;
  trackById = trackById;
  constructor(
    private route: ActivatedRoute,
    private messageService: MessageService,
    private dialog: MatDialog,
    private bar: MatSnackBar,
    private router: Router,
  ) {}

  ngOnInit() {
    this.route.data.subscribe((data: { result: { total: number, res: WorkMsg[] } }) => {
      this.workMsgs = data.result.res;
      this.total = data.result.total;
    });
    this.workMsg = {};

  }


  onPageChanged(page: number) {
    console.log(page);
    this.update(page).subscribe();
  }

  update(page: number) {
    // const options: any = { status };
    return this.messageService.getAllMessages(page, this.pageSize).pipe(tap((res) => {
      if (res.success) {
        this.currentPage = page;
        this.total = res.total;
        // this.companys = res.res;
        this.workMsgs = adjustList(res.res, 4);
      } else {
        this.bar.open(res.errorMsg);
      }
    }));
  }
}
