import { Component, Input, OnInit, EventEmitter, ElementRef, ViewChild } from '@angular/core';


@Component({
    selector: 'app-option',
    templateUrl: './appOption.html',
    styleUrls: ['./appOption.css']
})
export class AppOptionComponent implements OnInit {

    @Input()
    value: any;

    @ViewChild('display')
    display: ElementRef;

    selected: boolean;

    // 这个selectOption不能监听别的事件
    selectOption = new EventEmitter<any>();

    constructor() { }

    ngOnInit() {
    }

    onClickOption(val: any) {

        this.selectOption.emit(val);
    }

    getDisplay(): string {
        return this.display.nativeElement.innerText;
    }

}

