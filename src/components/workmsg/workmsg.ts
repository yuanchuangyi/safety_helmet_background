import { Component, OnInit, ViewChild, Inject, } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {MatDialogRef, MAT_DIALOG_DATA, MatDatepickerInputEvent, MatDialog} from '@angular/material';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { WorkProject } from '../../models/WorkProject';
import { WorkMsg } from '../../models/WorkMsg';

import { WorkjobService } from '../../service/WorkjobService';
import { WorkmsgService} from '../../service/WorkmsgService';

import { MsgAddComponent } from '../msgAdd/msgAdd';
import {ConfirmDialogComponent} from '../confirmDialog/confirmDialog';


@Component({
  templateUrl: './workmsg.html',
  styleUrls: ['./workmsg.css']
})
export class WorkmsgComponent implements OnInit {

  selection: SelectionModel<WorkMsg>;
  workmsgs: WorkMsg[];
  total: number;
  subscriptions: Subscription[] = [];
  pageSize = environment.pageSize;
  page = 0;
  workplace: WorkProject;
  projectId: number;
  cols: string[] = ['select', 'content', 'messageImg', 'createDate', 'operate'];
  haveMsg: boolean = true;
  constructor(
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<WorkmsgComponent>,
    private route: ActivatedRoute,
    private router: Router,
    private workjobService: WorkjobService,
    private workmsgService: WorkmsgService,
    @Inject(MAT_DIALOG_DATA) public data: {workplace: WorkProject}
  ) {
    this.workplace = data.workplace;
  }

  ngOnInit() {
    this.projectId = this.workplace.id;
    this.update(this.page, this.projectId).subscribe();
    const initialSelection = [];
    const allowMultiSelect = true;
    this.selection = new SelectionModel<WorkMsg>(allowMultiSelect, initialSelection);

  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.workmsgs.length;
    return numSelected === numRows;
  }

  allToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.workmsgs.forEach(row => this.selection.select(row));
  }




  update(page: number, projectId?: number) {
    const options: any = {  projectId };
    return this.workmsgService.getallmsg(page, this.pageSize, options).pipe(tap((res) => {
      if (res.success) {
        this.page = page;
        this.total = res.total;
        this.workmsgs = res.res;
        if (this.total === 0) {
          this.haveMsg = false;
        }
      }
    }));
  }

  onPageChanged(page: number) {
    this.update(page, this.projectId).subscribe();
  }

  trackById(index: number, item: any) {
    return item.id;
  }

  onOpenAdd() {
    const dialogRef = this.dialog.open( MsgAddComponent, {
      width: '500px',
      height: '420px',
      data: {
        projectId: this.projectId
      }
    });
    dialogRef.afterClosed().subscribe((res) => {
      this.update(this.page, this.projectId).subscribe();
    });
  }

  onDelete(id) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      height: '200px',
      data: {message: '确认删除？'}
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        alert('删除成功');
        this.workmsgService.deleteMsg(id).subscribe((res) => {
          if (res.success) {
            this.update(this.page, this.projectId).subscribe();
          }
        });
      }
    });
  }
}
