
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import {map, switchMapTo} from 'rxjs/operators';
import { environment } from '../environments/environment';

import { Company } from '../models/Company';

@Injectable()
export class CompanyService {
  private baseUrl = environment.apiUrl + 'api/company';

  constructor(
    private http: HttpClient
  ) {
  }

  delCompany(id): Observable<{success: boolean}> {
    return this.http.delete<{success: boolean}>(`${this.baseUrl}/del/${id}`);
  }

  insertCompany(company: Company): Observable<{res: Company, success: boolean}> {
    return this.http.post<{res: Company, success: boolean}>(`${this.baseUrl}/insert`, company);
  }

  getCompany(status:number = 2): Observable<{res: Company[], success: boolean}> {
    return this.http.get<{res: Company[], success: boolean}>(`${this.baseUrl}/my?status=${status}`);
  }

  updateCompany(company: Company): Observable<{res: Company, success: boolean}> {
    return this.http.patch<{res: Company, success: boolean}>(`${this.baseUrl}/update`, company);
  }

}
