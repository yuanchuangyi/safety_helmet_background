import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import {  TypeNameMap, statusType, StatusType } from '../../models/JobUser';
import { Workjob } from '../../models/Workjob';
import { WorkProject } from '../../models/WorkProject';
import { WorkLog } from '../../models/WorkLog';

import { environment } from '../../environments/environment';
import { ConfirmDialogComponent } from '../confirmDialog/confirmDialog';
import { WorkerRealinfoComponent } from '../workerRealinfo/workerRealinfo';

import { WorkplaceService } from '../../service/WorkplaceService';
import { WorkjobService } from '../../service/WorkjobService';
import { JobuserService } from '../../service/jobuserService';
import { AuthGuardService } from '../../service/AuthGuardService';
import { WorklogService } from '../../service/WorklogService';
import {WorkplaceMapComponent} from '../workplaceMap/workplaceMap';

declare var BMap: any;
@Component({
  templateUrl: './worklog.html',
  styleUrls: ['./worklog.css']
})
export class WorklogComponent implements OnInit {
  worklogs: WorkLog[];
  total: number;
  pageSize = environment.pageSize;
  page = 0;
  workplaces: WorkProject[];
  workjobs: Workjob[];
  StatusType = StatusType;
  TypeNameMap = TypeNameMap;
  status = StatusType.Applying;
  projectId: number;
  jobId: number;
  subscriptions: Subscription[] = [];
  selection: SelectionModel<WorkLog>;
  cols: string[] = ['select', 'username', 'title', 'imgurl', 'content', 'createDate', 'operate'];
  map: any;

  constructor(
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private router: Router,
    private  workplaceService: WorkplaceService,
    private workjobService: WorkjobService,
    private jobuserService: JobuserService,
    private worklogService: WorklogService,
    private authGuardService: AuthGuardService
  ) {
  }

  ngOnInit() {
    this.workplaceService.getmylist().subscribe((res) => {
      this.workplaces = res.res;
      this.projectId = this.workplaces[0].id;
      const status = this.status;
      const projectId = this.projectId;
      const options = {projectId, status};
      this.workjobService.getalljob(this.page, this.pageSize, options).subscribe((result) => {
        this.workjobs = result.res;
        this.jobId = null;
        // this.jobId = this.workjobs[0].id;
        this.update(this.page, this.pageSize, this.projectId, this.jobId);
      })
    });
    // this.map = new BMap.Map('allmap');
    const initialSelection = [];
    const allowMultiSelect = true;
    this.selection = new SelectionModel<WorkLog>(allowMultiSelect, initialSelection);
  }


  onProjectChange(projectId: number) {
    const status = this.status;
    const options: any = {projectId, status};
    this.workjobService.getalljob(this.page, this.pageSize, options).subscribe((res) => {
      this.workjobs = res.res;
      this.jobId = null;
      // this.jobId = this.workjobs[0].id;
      this.projectId = projectId;
      this.update(this.page, this.pageSize, this.projectId, this.jobId);
    })
  }


  onJobChange(jobid: number) {
    this.jobId = jobid;
    this.update(this.page, this.pageSize, this.projectId, this.jobId);
  }


  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.worklogs.length;
    return numSelected === numRows;
  }

  allToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.worklogs.forEach(row => this.selection.select(row));
  }

  update(pageIndex: number, pageSize: number, projectId?: number, jobId?: number) {
    const options: any = {jobId, projectId};
    this.worklogService.getlog(pageIndex, pageSize, options).subscribe((res) => {
      this.worklogs = res.res;
      this.total = res.total;
      this.page = pageIndex;
    });
  }

  trackById(index: number, item: any) {
    return item.id;
  }

  openMsgplace(workplace) {
    const dialogRef = this.dialog.open(WorkplaceMapComponent, {
      width: '800px',
      height: '400px',
      data: {
        workplace: workplace
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.update(this.page, this.pageSize, this.projectId, this.jobId);
    });
  }
}
