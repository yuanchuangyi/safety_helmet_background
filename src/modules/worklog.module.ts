import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ShareModule } from './share.module';
import { WorklogComponent } from '../components/worklog/worklog';

import { WorklogRoutingModule } from '../router/worklog';
import { WorklogService } from '../service/WorklogService';

@NgModule({
  imports: [
    BrowserModule,
    ShareModule,
    WorklogRoutingModule
  ],
  declarations: [
    WorklogComponent
  ],
  providers: [
    WorklogService
  ],
  entryComponents: [
    WorklogComponent
  ]
})
export class WorklogModule { }
