import { Component, OnInit } from '@angular/core';

import { Article } from '../../../src/models/Article';
import { ArticleService } from '../../service/ArticleService';

import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../src/environments/environment';
import { tap } from 'rxjs/operators';
import { MatDialog, MatSnackBar } from '@angular/material';

import { CheckStatus,  CheckStatusMap} from  '../../../src/models/Company';
import { adjustList, trackById } from '../../../src/others/utils';

@Component({
  templateUrl: './articleList.html',
  styleUrls: ['./articleList.css']
})
export class ArticleListComponent implements OnInit {

  CheckStatus = CheckStatus;
  CheckStatusMap = CheckStatusMap;
  status: CheckStatus;
  articles: Article[];
  total: number;
  currentPage: number = 1;
  pageSize: number = environment.adminPageSize;
  trackById = trackById;
  environment = environment;
  constructor(
    private articleService: ArticleService,
    private route: ActivatedRoute,
    private router: Router,
    private bar: MatSnackBar,
    private dialog: MatDialog,
  ) {

  }

  ngOnInit() {
    this.route.data.subscribe((data: {result: {res: Article[], total: number}}) => {
      this.articles = adjustList(data.result.res, 4);
      // this.companys = data.result.res;
      this.total = data.result.total;
    });

    const url = this.router.routerState.snapshot.url;
    if (url.indexOf('published') > 0) {
      this.status = CheckStatus.PASS;
    } else if (url.indexOf('unpassed') > 0) {
      this.status = CheckStatus.UNPASS;
    } else if (url.indexOf('undershelf') > 0) {
      this.status = CheckStatus.UNDERSHELF;
    } else  {
      this.status = CheckStatus.UNREVIEW;
    }
  }

  onPageChanged(page: number) {
    this.update(page, this.status).subscribe();
  }


  update(page: number, status?: CheckStatus) {
    const options: any = { status };
    return this.articleService.getAllArticles(page, this.pageSize, options).pipe(tap((res) => {
      if (res.success) {
        this.currentPage = page;
        this.total = res.total;
        // this.companys = res.res;
        this.articles = adjustList(res.res, 4);
      } else {
        this.bar.open(res.errorMsg);
      }
    }));
  }

}
