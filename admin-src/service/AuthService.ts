
import { Injectable } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';
import { map, catchError, tap  } from 'rxjs/operators';

import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';

import { User } from '../../src/models/User';
import { environment } from '../environments/environment';

@Injectable()
export class AuthService {

  user: User;
  // store the URL so we can redirect after logging in
  redirectUrl: string;

  private baseUrl =  environment.apiUrl + 'api';

  userSource = new Subject<User>();
  userObservable = this.userSource.asObservable();

  constructor(
    private http: HttpClient
  ) {

  }

  // login(user: User): Observable<{user: User, success: boolean, isValidateCodeLogin: boolean, errorMsg: string}> {
  //   return this.http.post<{user: User, isValidateCodeLogin: boolean, success: boolean, errorMsg: string}>(this.baseUrl + '/login', user).pipe(tap((res) => {
  //     if (res.success) {
  //       this.userSource.next(res.user);
  //     }
  //   }));
  // }

  login(user: User): Observable<{ res: User, success: boolean, isValidateCodeLogin: boolean, errorMsg: string }> {
    const paramsObj: any = {
      username: String(user.username),
      password: String(user.mima)
    };
    if (user.validateCode) paramsObj.validateCode = user.validateCode;
    const params = new HttpParams({ fromObject: paramsObj });
    return this.http.get<{ res: User, success: boolean, isValidateCodeLogin: boolean, errorMsg: string }>(this.baseUrl + '/login',  { params: params }).pipe(tap((res) => {
      if (res.success) {
        this.userSource.next(res.res);
      }
    }));
  }
  logout(): Observable<any> {
    this.user = null;
    this.userSource.next(null);
    return this.http.get(this.baseUrl + '/login/logout');
  }

  getCurrent(): Observable< {res: User, success: boolean}> {
    return this.http.get<{res: User, success: boolean}>(this.baseUrl + '/sysuser/getuserbysession').pipe(map(res => {
      if (res.success) {
        this.user = res.res;
        this.userSource.next(this.user);
      }
      return res;
    }));
  }

  getCode(): Observable<any> {
    return this.http.get(this.baseUrl + '/login/validateCode');
  }

  isAdmin(user: User): boolean {
    return !!user && user.username === 'admin';
  }

  checkRole(): Observable< {res: User, success: boolean, hasRoleUser: boolean, hasRoleAdmin: boolean, isAuthenticated: boolean, isRemember: boolean}> {
    return this.http.get<{res: User, success: boolean, hasRoleUser: boolean, hasRoleAdmin: boolean, isAuthenticated: boolean, isRemember: boolean}>
    (this.baseUrl + '/api/login/checkRoleAndPermission');
  }
}
