import { Component, OnInit, Inject, AfterViewChecked, ViewChild } from '@angular/core';
import {FormControl, NgForm} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDatepickerInputEvent } from '@angular/material';
import { Moment } from 'moment';
import * as moment from 'moment';
import { WorkProject } from '../../models/WorkProject';
import { WorkplaceService } from '../../service/WorkplaceService';

import { Workjob } from '../../models/Workjob';
import { WorkjobService } from '../../service/WorkjobService';
import { Company } from '../../models/Company';

@Component({
  templateUrl: './postJob.html',
  styleUrls: ['./postJob.css']
})
export class  PostJobComponent implements OnInit, AfterViewChecked {

  messages = {
    jobName: '',
    description: '',
    salary: '',
    peopleNum: '',
    workAge: '',
  };
  workplace: WorkProject;
  workjob: Workjob;
  company: Company;
  constructor(
    private dialogRef: MatDialogRef<PostJobComponent>,
    private workplaceService: WorkplaceService,
    private workjobService: WorkjobService,
    @Inject(MAT_DIALOG_DATA) public data: {workplace: WorkProject, company: Company, workjob: Workjob}
  ) {
    this.workplace = data.workplace || {};
    this.company = data.company || {};
    this.workjob = data.workjob || {};
  }

  @ViewChild('workjobForm')
  workjobForm: NgForm;

  ngAfterViewChecked() {
    if (this.workjobForm) {
      this.workjobForm.valueChanges.subscribe(data => this.checkValidate());
    }
  }
  ngOnInit() {

  }

  onSubmit() {
    this.checkValidate(true);
    this.workjob.status = 1;
    if (this.workjobForm.invalid) {
      // 触发mat的error
      this.workjobForm.onSubmit(null);
      return false;
    }
    if (this.workjob.id !== undefined) {
      delete this.workjob.createDate;
      delete  this.workjob.updateDate;
      this.workjobService.updateJob(this.workjob).subscribe((res) => {
        alert('修改成功!');
        this.dialogRef.close(res.res);
      })
    } else {
        this.workjob.projectId = this.workplace.id;
        this.workjob.companyId = this.company.id;
        this.workjobService.insertJob(this.workjob).subscribe((res) => {
          alert('发布成功！');
          this.dialogRef.close(res.res);
        })
    }

  }



  checkValidate(isSubmit?: boolean) {
    const form = this.workjobForm;

    for (const field in PostJobComponent.ErrorMessages) {

      const control = form.controls[field];

      if (control && control.valid) {
        this.messages[field] = '';
      }
      if (control && (control.dirty || isSubmit) && !control.valid) {
        const messages = PostJobComponent.ErrorMessages[field];
        if (control.errors) {
          this.messages[field] = messages[Object.keys(control.errors)[0]];
        }
      }

    }
  }

  static ErrorMessages = {
    jobName: {
      required: '职位名称不能为空'
    },
    description: {
      required: '描述不能为空'
    },
    salary: {
      required: '薪水不能为空',
      pattern: '格式不正确，只能输入数字'
    },
    peopleNum: {
      required: '需求人数不能为空',
      pattern: '格式不正确，只能输入数字'
    },
    workAge: {
      required: '工龄不能为空',
      pattern: '格式不正确，只能输入数字'
    }
  };
}
