import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RealnameManageComponent} from '../components/realnameManage/realnameManage';
import { RealnameDetailComponent } from '../components/realnameDetail/realnameDetail';
import { RealinfoResolveService } from '../service/RealinfoResolveService';
import { AuthGuardService } from '../service/AuthGuardService';


const routes: Routes = [
  {
    path: 'realinfo',
    canActivate: [AuthGuardService],
    children: [
      {
        path: '',
        redirectTo: 'unreview',
        pathMatch: 'full'
      },
      {
        path: 'unreview',
        component: RealnameManageComponent,
        resolve: {
          result: RealinfoResolveService
        }
      },
      {
        path: 'published',
        component: RealnameManageComponent,
        resolve: {
          result: RealinfoResolveService
        }
      },
      {
        path: 'unpassed',
        component: RealnameManageComponent,
        resolve: {
          result: RealinfoResolveService
        }
      },
      {
        path: 'undershelf',
        component: RealnameManageComponent,
        resolve: {
          result: RealinfoResolveService
        }
      },
      {
        path: 'realinfoDetail/:id',
        component: RealnameDetailComponent,
        resolve: {
          result: RealinfoResolveService
        }
      }
    ]
  },
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class RealinfoRoutingModule {}
