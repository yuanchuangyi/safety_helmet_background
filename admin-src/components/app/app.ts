import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

// import { Router, RouterEvent, NavigationEnd } from '@angular/router';

@Component({
  selector: 'body',
  templateUrl: 'app.html',
  styleUrls: ['app.css']
})
export class AppComponent implements OnInit {


  constructor(private titleService: Title) {
  }

  ngOnInit() {

    this.titleService.setTitle( '安全帽管理员后台' );



  }




}
