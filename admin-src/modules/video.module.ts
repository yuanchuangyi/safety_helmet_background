import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ShareModule } from './share.module';

import { VideoRoutingModule } from '../router/video';
import { VideoService } from '../service/VideoService';
import { VideoResolveService } from '../service/VideoResolveService';
import { VideoManageComponent } from '../components/videoManage/videoManage';
import { VideoDetailComponent } from '../components/videoDetail/videoDetail';

@NgModule({
  imports: [BrowserModule, ShareModule, VideoRoutingModule],
  declarations: [
    VideoManageComponent,
    VideoDetailComponent
  ],
  providers: [
    VideoService,
    VideoResolveService
  ]
})
export class VideoModule { }
