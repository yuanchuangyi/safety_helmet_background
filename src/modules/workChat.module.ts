import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ShareModule } from './share.module';

import { WorkChatComponent } from '../components/workChat/workChat';

import { WorkChatRoutingModule } from '../router/workChat';

@NgModule({
  imports: [
    BrowserModule,
    ShareModule,
    WorkChatRoutingModule
  ],
  declarations: [
    WorkChatComponent
  ],
  providers: [

  ],
  entryComponents: [
    WorkChatComponent
  ]
})
export class WorkChatModule { }
