import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomePageComponent } from '../components/homePage/homePage';

const routes: Routes = [
  // {
  //   path: 'home',
  //   component: HomePageComponent,
  // }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ],
  providers: [

  ]

})
export class HomeRoutingModule {}
