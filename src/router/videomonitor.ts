import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { VideoMonitorComponent } from '../components/videoMonitor/videoMonitor';
import {AuthGuardService} from '../service/AuthGuardService';

const routes: Routes = [
  {
    path: 'videoMonitor',
    component: VideoMonitorComponent,
    canActivate: [AuthGuardService],
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ],
})
export class VideomonitorRoutingModule {}
