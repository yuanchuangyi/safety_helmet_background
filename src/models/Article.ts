import {User} from './User';
export class Article {
  checkMsg?: string;
  clickNum?: number;
  commentNum?: number;
  content?: string;
  createUserId?: number;
  description?: string;
  favorite?: number;
  id?: number;
  isFavo?: boolean;
  isLike?: boolean;
  keyWords?: string;
  likes?: number;
  otherProperty?: string;
  shareNum?: number;
  status?: number;
  title?: string;
  type?: number;
  updateUserid?: number;
  url?: string;
  userId?: number;
  sysUser?: User;
  updateDate?: Date;
  createDate?: Date;
}
