import { NgModule } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { ShareModule } from './share.module';
import { HomeModule} from './home.module';
import { AuthModule } from './auth.module';
import { ArticleModule } from './article.module';
import { WorkplaceModule } from './workplace.module';
import { VideoModule } from './video.module';
import { RealinfoModule } from './realinfo.module';
import { MessageModule } from './message.module';
import { WorkerTopModule } from './workerTop.module';
import { AppRoutingModule } from '../router/app';
import { MatSnackBar } from '@angular/material';

import { AppComponent } from '../components/app/app';
import { NavComponent } from '../components/nav/nav';
import { HomeComponent} from '../components/home/home';
import { CompanyDetailComponent } from '../components/companyDetail/companyDetail';

import { AuthService } from '../service/AuthService';
import { AuthGuardService } from '../service/AuthGuardService';


@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    HomeModule,
    ShareModule,
    AppRoutingModule,
    AuthModule,
    WorkplaceModule,
    ArticleModule,
    VideoModule,
    RealinfoModule,
    MessageModule,
    WorkerTopModule
  ],
  declarations: [
    AppComponent,
    NavComponent,
    HomeComponent,
    CompanyDetailComponent
  ],
  providers: [
    Title,
    AuthService,
    AuthGuardService,
    MatSnackBar
  ],
  bootstrap:    [ AppComponent ],

})
export class AppModule { }
