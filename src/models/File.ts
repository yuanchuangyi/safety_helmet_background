/**
 * 对应 resResource 对象
 */

export interface File {
  id?: string;
  fullname?: string;
  name?: string;
  url?: string;
  format?: string;
  filesize?: number;
  size?: number;
  filetype?: string;
  metaData?: any;
  status?: number;
  createUserid?: string;
  selected?: boolean;
  createDate?: Date;
  updatedate?: Date;

}
