import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../service/AuthService';
import { User } from '../../../src/models/User';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.html',
  styleUrls: ['./nav.css']
})
export class NavComponent implements OnInit {

  user: User;
  constructor (
    private authService: AuthService
  ) {

  }
  ngOnInit() {
    this.user = this.authService.user;
  }

}
