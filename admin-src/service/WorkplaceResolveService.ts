import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { WorkProject } from '../../src/models/WorkProject';
import { WorkplaceService } from './WorkplaceService';


import { environment } from '../../src/environments/environment';

import { CheckStatus } from '../../src/models/Company';

@Injectable()
export class WorkplaceResolveService implements Resolve<{ res: WorkProject[], total: number}> {

  constructor(private workplaceService: WorkplaceService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{ res: WorkProject[], total: number}>  {

    let status: CheckStatus;
    if (state.url.indexOf('published') > 0) {
      status = CheckStatus.PASS;
    } else if (state.url.indexOf('unpassed') > 0) {
      status = CheckStatus.UNPASS;
    } else if (state.url.indexOf('undershelf') > 0) {
      status = CheckStatus.UNDERSHELF;
    } else  {
      status = CheckStatus.UNREVIEW;
    }
    return this.workplaceService.getAllWorkplaces(1, environment.adminPageSize, { status }).pipe(map((res) => {
      if (res.success) {
        return  res;
      } else {
        console.error(res.errorMsg);
      }
    }));
  }
}
