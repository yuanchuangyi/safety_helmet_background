import { Component, OnInit, Inject, AfterViewChecked, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDatepickerInputEvent } from '@angular/material';
import { Moment } from 'moment';
import * as moment from 'moment';
import { WorkProject } from '../../models/WorkProject';
import { WorkplaceService } from '../../service/WorkplaceService';
import { Company } from '../../models/Company';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import {environment} from '../../environments/environment';

@Component({
  templateUrl: './workplaceAdd.html',
  styleUrls: ['./workplaceAdd.css']
})
export class  WorkplaceAddComponent implements OnInit, AfterViewChecked {
  uploader: FileUploader = new FileUploader({
    url: environment.apiUrl + 'api/fileupload/img',
    removeAfterUpload: true,
    method: 'POST'
  });
  messages = {
    projectCode: '',
    projectName: '',
    description: '',
    projectAddress: '',
    projectPhone: '',
    totalsWorkers: '',
    nowWorkers: ''
  };
  workplace: WorkProject;
  company: Company;
  beginCtrl = new FormControl();
  endCtrl = new FormControl();
  imgUrls: string;
  imgUrlsArr: string[] = [];
  constructor(
    private dialogRef: MatDialogRef<WorkplaceAddComponent>,
    private workplaceService: WorkplaceService,
    // private companyService: CompanyService,
    @Inject(MAT_DIALOG_DATA) public data: { workplace: WorkProject, company: Company }
  ) {
    this.company = data.company;
    this.workplace = data.workplace || new WorkProject();
    this.workplace.startDateShow = new FormControl(moment(this.workplace.startDate));
    this.workplace.startDate = moment(this.workplace.startDate).toDate();
    this.workplace.endDateShow = new FormControl(moment(this.workplace.endDate));
    this.workplace.endDate = moment(this.workplace.endDate).toDate();
  }

  @ViewChild('workplaceForm')
  workplaceForm: NgForm;

  ngAfterViewChecked() {
    if (this.workplaceForm) {
      this.workplaceForm.valueChanges.subscribe(data => this.checkValidate());
    }
  }
  ngOnInit() {
    if (this.workplace.imgUrls) {
      this.imgUrlsArr = this.workplace.imgUrls.split(',');
    }
  }

  onSubmit() {
    this.checkValidate(true);
    if (this.workplaceForm.invalid) {
      // 触发mat的error
      this.workplaceForm.onSubmit(null);
      return false;
    }
    if (this.workplace.id !== undefined) {
        this.workplaceService.updateProject(this.workplace).subscribe((res) => {
          this.dialogRef.close(res.res);
        })
    } else {
      this.workplace.companyID = this.company.id;
        this.workplaceService.insertProject(this.workplace).subscribe((res) => {
          this.dialogRef.close(res.res);
        })
    }
  }

  onSelectbeginDate( event: MatDatepickerInputEvent<Moment>) {
    this.workplace.startDate = event.value.toDate();
    this.workplace.startDateShow = new FormControl(event.value);
  }
  onSelectendDate( event: MatDatepickerInputEvent<Moment>) {
    this.workplace.endDate = event.value.toDate();
    this.workplace.endDateShow = new FormControl(event.value);
  }

  checkValidate(isSubmit?: boolean) {
    const form = this.workplaceForm;

    for (const field in WorkplaceAddComponent.ErrorMessages) {

      const control = form.controls[field];

      if (control && control.valid) {
        this.messages[field] = '';
      }
      if (control && (control.dirty || isSubmit) && !control.valid) {
        const messages = WorkplaceAddComponent.ErrorMessages[field];
        if (control.errors) {
          this.messages[field] = messages[Object.keys(control.errors)[0]];
        }
      }
    }
  }

  ImgsOnChanged(event: any) {
    this.uploader.queue.forEach(queue => {
      queue.withCredentials = false;
      queue.onError = (response: string, status: number) => {
        console.log(response, status);
      };

      queue.onSuccess = (responses, status, headers) => {
        if (status === 200) {
          const response = JSON.parse(responses);
          // this.workplace.imgUrls =  response['full-url'];
          if (this.imgUrlsArr.length < 4) {
            this.imgUrlsArr.push(response['full-url']);
            this.workplace.imgUrls = String(this.imgUrlsArr);
          } else {
            alert('最多只能上传四张');
          }
        } else {
          console.log('err', Error);
        }
      };
      queue.upload();
    });
  }

  deleteimg(index) {
      this.imgUrlsArr.splice(index, 1);
      this.workplace.imgUrls = String(this.imgUrlsArr);
  }
  static ErrorMessages = {
    projectCode: {
      required: '工程编号不能为空'
    },
    projectName: {
      required: '工程名称不能为空'
    },
    description: {
      required: '工地简介不能为空'
    },
    projectAddress: {
      required: '工程地址不能为空'
    },
    projectPhone: {
      pattern: '手机号格式不正确',
      required: '工程手机号不能为空'
    },
    totalsWorkers: {
      pattern: '格式不正确，只能输入数字'
    },
    nowWorkers: {
      pattern: '格式不正确，只能输入数字'
    }
  };
}
