import { Component, ViewChild, AfterViewChecked, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MatDatepickerInputEvent } from '@angular/material';
import { tap, filter, switchMapTo } from 'rxjs/operators';
import { interval } from 'rxjs';
import { AuthService } from '../../service/AuthService';
import { UserService } from '../../service/UserService';
import { User } from '../../models/User';
import { UserRealinfo, PaperType, paperType, TypeNameMap } from '../../models/UserRealinfo';
import { ActivatedRoute } from '@angular/router';
import { UserinfoService } from '../../service/UserinfoService';
import { Moment } from 'moment';
import { FormControl } from '@angular/forms';
import * as moment from 'moment';
import { environment } from '../../environments/environment';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
@Component({
  templateUrl: 'realnameProve.html',
  styleUrls: ['realnameProve.css'],
})


export class RealnameProveComponent implements AfterViewChecked, OnInit {
  uploader: FileUploader = new FileUploader({
    url: environment.apiUrl + 'api/fileupload/img',
    removeAfterUpload: true,
    method: 'POST'
  });
  public data; any;
  paperType = paperType;
  TypeNameMap = TypeNameMap;
  user: User;
  loading: Boolean = false;
  environment = environment;
  haveRealinfo: boolean = false;
  messages = {
    realName: '',
    address: '',
    personNum: '',
    certificateAuthority: ''
  };
  userRealinfo: UserRealinfo;
  @ViewChild('proveForm')
  proveForm: NgForm;

  ngAfterViewChecked() {
    // Angular的“单向数据流”规则禁止在一个视图已经被组合好之后再更新视图.而此时已组合好
    if (this.proveForm) {
      this.proveForm.valueChanges.subscribe(data => this.checkValidate());
    }
  }

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private router: Router,
    public route: ActivatedRoute,
    private userinfoService: UserinfoService,

  ) {

  }
  ngOnInit() {
    this.user = new User();
    this.userRealinfo = new UserRealinfo();
    this.userRealinfo.validityDateShow = new FormControl(moment(this.userRealinfo.validityDate));
    this.userRealinfo.validityDate = moment(this.userRealinfo.validityDate).toDate();
    this.route.queryParams.subscribe(params => {
      this.data = params['userid'];
    })
    this.userinfoService.getoneInfo(this.data).subscribe((res) => {
      if (res.res) {
        this.userRealinfo = res.res;
        this.haveRealinfo = true;
      }
    })

    // this.userRealinfo.createDateShow = new  FormControl(moment(this.userRealinfo.createDate));
    // this.userRealinfo.createDate = moment(this.userRealinfo.createDate).toDate();

  }
  // SelectCreateDate( event: MatDatepickerInputEvent<Moment>) {
  //   this.userRealinfo.createDate = event.value.toDate();
  //   this.userRealinfo.createDateShow = new FormControl(event.value);
  // }

  SelectValidityDate( event: MatDatepickerInputEvent<Moment>) {
    this.userRealinfo.validityDate = event.value.toDate();
    this.userRealinfo.validityDateShow = new FormControl(event.value);
  }

  prove() {
    this.checkValidate(true);
    if (this.proveForm.invalid) {
      return false;
    }
    this.userRealinfo.status = 0;
    this.userRealinfo.userid = this.data;
    this.userinfoService.insertInfo(this.userRealinfo).subscribe((res) => {
      if (res.success) {
        this.router.navigate(['/home']);
      }
    });
  }

  return() {
    this.router.navigate(['/personInfo']);
  }


  PositiveOnChanged(event: any) {
    this.uploader.queue.forEach(queue => {
      queue.withCredentials = false;
      queue.onError = (response: string, status: number) => {
        console.log(response, status);
      };

      queue.onSuccess = (responses, status, headers) => {
        if (status === 200) {
          const response = JSON.parse(responses);
          this.userRealinfo.cardUrl1 =  response['full-url'];
        } else {
          console.log('err', Error);
        }
      };
      queue.upload();
    });
  }
  OppisiteOnChanged (event: any) {
    this.uploader.queue.forEach(queue => {
      queue.withCredentials = false;
      queue.onError = (response: string, status: number) => {
        console.log(response, status);
      };

      queue.onSuccess = (responses, status, headers) => {
        if (status === 200) {
          const response = JSON.parse(responses);
          this.userRealinfo.cardUrl2 =  response['full-url'];
        } else {
          console.log('err', Error);
        }
      };
      queue.upload();
    });
  }

  checkValidate(isSubmit?: boolean) {
    const form = this.proveForm;

    for (const field in RealnameProveComponent.ErrorMessages) {

      const control = form.controls[field];

      if (control && control.valid) {
        this.messages[field] = '';
      }
      if (control && (control.dirty || isSubmit) && !control.valid) {
        const messages = RealnameProveComponent.ErrorMessages[field];
        if (control.errors) {
          this.messages[field] = messages[Object.keys(control.errors)[0]];
        }
      }

    }
  }

  static ErrorMessages = {
    realName: {
      required: '姓名不能为空',
    },
    address: {
      required: '籍贯不能为空'
    },
    personNum: {
      required: '身份证号不能为空',
      pattern: '输入的格式不正确'
    },
    certificateAuthority: {
      required: '证件机关不能为空'
    }
  };

}
