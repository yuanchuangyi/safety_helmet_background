import { NgModule } from '@angular/core';
import { ShareModule } from './share.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AuthService } from '../service/AuthService';
import { AuthGuardService } from '../service/AuthGuardService';
import { UserService } from '../service/UserService';

import { LoginComponent } from '../components/login/login';

import { FileUploadModule } from 'ng2-file-upload';
@NgModule({
  imports: [
    ShareModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule
  ],
  declarations: [
    LoginComponent,
  ],
  providers: [
    AuthGuardService,
    AuthService,
    UserService
  ],
  exports: [
    LoginComponent,
  ]
})
export class AuthModule {}
