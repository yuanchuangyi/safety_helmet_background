import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../../src/models/User';
import { WorkerTopService } from './WorkerTopService';

import { environment } from '../../src/environments/environment';

import { CheckStatus } from '../../src/models/Company';

@Injectable()
export class WorkertopResolveService implements Resolve<{ res: User[], total: number}> {

  constructor(private workerTopService: WorkerTopService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{ res: User[], total: number}>  {
    return this.workerTopService.getAllWorkertops(1, environment.adminPageSize).pipe(map((res) => {
      if (res.success) {
        return  res;
      } else {
        console.error(res.errorMsg);
      }
    }));
  }
}
