import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import * as $ from 'jquery';

/**
 * Async modal dialog service
 * DialogService makes this app easier to test by faking this service.
 */

@Injectable()
export class DialogService {

  time = 1000; // ms
  tipHandler: any;
  tipHtml: any;

  constructor(

  ) {


  }

  /**
   * Ask user to confirm an action. `message` explains the action and choices.
   * Returns promise resolving to `true`=confirm or `false`=cancel
   */
  confirm(option: {message: string, ok?: string, cancel?: string }): Observable<boolean> {
    const clickStream = new Subject<boolean>();

    const html = $(`
             <div class="popup-bg">
                <div class="popup">
                    <div class="middle">
                        <span>${option.message}</span>
                    </div>
                    <div class="bottom">
                        <button class="am-btn am-material-btn" id="popup_ok" >${option.ok || '确定'}</button>
                        <button class="am-btn am-material-cancel-btn" id="popup_cancel" >${option.cancel || '取消'}</button>
                    </div>
                </div>
             </div>
        `);

    html.on('click', '#popup_ok', (event) => {
      html.remove();
      clickStream.next(true);
    });
    html.on('click', '#popup_close, #popup_cancel', (event) => {
      html.remove();
      clickStream.next(false);
    });

    $('body').append(html);

    return clickStream;
  }

  alert(message?: string): Observable< any> {
    const clickStream = new Subject<any>();
    const carrierHtml = $('body');

    const html = $(`
             <div class="popup-bg">
                <div class="popup">
                    <div class="middle">
                       <span>${message}</span>
                    </div>
                    <div class="bottom">
                         <button class="am-btn am-material-btn" id="popup_ok" >确定</button>
                    </div>
                </div>
             </div>
        `);
    html.on('click', '#popup_ok, #popup_close', (event) => {
      html.remove();
      clickStream.next(event);

    });
    if ( carrierHtml.find('.popup-bg').length === 0 ) {
      carrierHtml.append(html);
    }
    return clickStream;

  }



  tip(message?: string) {

    if (this.tipHandler) {

      clearTimeout(this.tipHandler);
      // 删除
      this.tipHtml.remove();
      this.tipHtml = null;

    }

    // 插入
    this.tipHtml = $(`
             <div class="pop-tip">
                  <span>${message}</span>
             </div>
        `);

    $('body').append(this.tipHtml);

    this.tipHandler = setTimeout(() => {

      // 删除
      this.tipHtml.remove();

    }, this.time);
  }

  dialog(message: string) {

  }

  loading() {

  }
}
