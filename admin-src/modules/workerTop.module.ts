import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ShareModule } from './share.module';
import { WorkertopResolveService} from '../service/WorkertopResolveService';
import { WorkerTopComponent } from '../components/workerTop/workerTop';
import { WorkerTopService } from '../service/WorkerTopService';
import { WorkerTopRoutingModule} from '../router/workerTop';

@NgModule({
  imports: [BrowserModule, ShareModule, WorkerTopRoutingModule],
  declarations: [
    WorkerTopComponent,

  ],
  providers: [
    WorkerTopService,
    WorkertopResolveService
  ]
})
export class WorkerTopModule { }
