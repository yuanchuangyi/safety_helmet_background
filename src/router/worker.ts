import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { WorkerListComponent } from '../components/workerList/workerList';
import { AuthGuardService } from '../service/AuthGuardService';
import { UsersResolveService } from '../service/UsersResolveService';

const routes: Routes = [
  {
    path: 'worker',
    canActivate: [AuthGuardService],
    component: WorkerListComponent,
    resolve: {
      results: UsersResolveService
    }
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ],


})
export class WorkerRoutingModule {}
