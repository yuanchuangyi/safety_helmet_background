import { Component, ViewChild, AfterViewChecked } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { NgForm } from '@angular/forms';

import { AuthService } from '../../service/AuthService';
import { User } from '../../../src/models/User';


@Component({
  templateUrl: 'login.html',
  styleUrls: ['login.css'],
})
export class LoginComponent implements AfterViewChecked {
  static ErrorMessages = {
    username: {
      required: '用户名不能为空',
      notExist: '用户名不存在'
    },
    mima: {
      required: '密码不能为空',
      unMatched: '用户名密码不匹配',
      notAdmin: '只有管理员才有权限登录'
    },
    validateCode: {
      required: '验证码不能为空',
      unMatched: '验证码错误',
    }
  };
  user: User;
  codeimg: any;
  messages = {
    username: '',
    mima: '',
    validateCode: ''
  };
  isValidateCodeLogin: boolean = false;
  hasimg: boolean = true;
  randomNumber: number;
  @ViewChild('loginForm')
  loginForm: NgForm;
  baseurl: string = 'http://file.sxycy.cn:84/api/login/validateCode?';
  constructor(public authService: AuthService,
              public router: Router) {
    this.user = new User();
  }
  ngAfterViewChecked() {

    // Angular的“单向数据流”规则禁止在一个视图已经被组合好之后再更新视图.而此时已组合好
    if (this.loginForm) {
      this.loginForm.valueChanges.subscribe(data => this.checkValidate());
    }
  }
  login() {
    this.checkValidate(true);
    if (this.loginForm.invalid) {
      return false;
    }
    this.authService.login(this.user).subscribe((res) => {
      if (res.isValidateCodeLogin) {
        this.isValidateCodeLogin = true;
      }
      if (res.success && this.authService.isAdmin(res.res)) {
        this.authService.user = res.res;
        const redirect: any[] = this.authService.redirectUrl ? [this.authService.redirectUrl]
          : ['/home'];
        const navigationExtras: NavigationExtras = {
          queryParamsHandling: 'preserve',
          preserveFragment: true
        };

        this.router.navigate(redirect, navigationExtras);
      }
      else if (res.success && !this.authService.isAdmin(res.res)) {
        this.messages.mima = LoginComponent.ErrorMessages.mima.notAdmin;
      }
      else if (!res.success){
        this.messages.mima = LoginComponent.ErrorMessages.mima.unMatched;
      }
    });
  }

  changeimg() {
    this.randomNumber = Number(1000 * Math.random());
    this.hasimg = false;
    setTimeout( () => {
      this.hasimg = true;
    },100);
  }

  checkValidate(isSubmit?: boolean) {
    const form = this.loginForm;

    for (const field in LoginComponent.ErrorMessages) {

      const control = form.controls[field];

      if (control && control.valid) {
        this.messages[field] = '';
      }
      if (control && (control.dirty || isSubmit) && !control.valid) {
        const messages = LoginComponent.ErrorMessages[field];
        if (control.errors) {
          this.messages[field] = messages[Object.keys(control.errors)[0]];
        }
      }
    }
  }

}
