import { Component, OnInit } from '@angular/core';
import { Company } from '../../../src/models/Company';
import { CompanyService} from '../../service/CompanyService';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../src/environments/environment';
import { tap } from 'rxjs/operators';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ConfirmDialogComponent } from '../../../src/components/confirmDialog/confirmDialog';
import { CheckStatus,  CheckStatusMap} from  '../../../src/models/Company';
import { adjustList, trackById } from '../../../src/others/utils';

@Component({
  templateUrl: './home.html',
  styleUrls: ['./home.css']
})
export class HomeComponent implements OnInit {

  CheckStatus = CheckStatus;
  CheckStatusMap = CheckStatusMap;
  status: CheckStatus;
  companys: Company[];
  total: number;
  currentPage: number = 1;
  pageSize: number = environment.adminPageSize;
  trackById = trackById;

  constructor(
    private companyService: CompanyService,
    private route: ActivatedRoute,
    private router: Router,
    private bar: MatSnackBar,
    private dialog: MatDialog,
  ) {

  }

  ngOnInit() {
    this.route.data.subscribe((data: {result: {res: Company[], total: number}}) => {
      this.companys = adjustList(data.result.res, 4);
      // this.companys = data.result.res;
      this.total = data.result.total;
    });

    const url = this.router.routerState.snapshot.url;
    if (url.indexOf('published') > 0) {
      this.status = CheckStatus.PASS;
    } else if (url.indexOf('unpassed') > 0) {
      this.status = CheckStatus.UNPASS;
    } else if (url.indexOf('undershelf') > 0) {
      this.status = CheckStatus.UNDERSHELF;
    } else  {
      this.status = CheckStatus.UNREVIEW;
    }
  }

  onPageChanged(page: number) {
    this.update(page, this.status).subscribe();
  }


  update(page: number, status?: CheckStatus) {
    const options: any = {  status };
    return this.companyService.getAllCompanys(page, this.pageSize, options).pipe(tap((res) => {
      if (res.success) {
        this.currentPage = page;
        this.total = res.total;
        // this.companys = res.res;
        this.companys = adjustList(res.res, 4);
      } else {
        this.bar.open(res.errorMsg);
      }
    }));
  }



}
