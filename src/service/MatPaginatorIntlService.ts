/**
 * Created by jixianyan on 2018/5/29.
 */

import {MatPaginatorIntl} from '@angular/material';

export class MatPaginatorIntlService extends MatPaginatorIntl {
  itemsPerPageLabel = '每页条数';
  nextPageLabel     = '下一页';
  previousPageLabel = '上一页';
  firstPageLabel = '首页';
  lastPageLabel = '末页';

  getRangeLabel = function (page, pageSize, length) {
    return `总共${length}条数据`;
  };

}
