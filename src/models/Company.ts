import { User } from './User';

export class Company {
  companyAddress?: string;
  companyCorporation?: string;
  companyName?: string;
  companyNum?: string;
  companyPhone?: string;
  description?: string;
  employeeNum?: number;
  id?: number;
  createUserId?: number;
  type?: string;
  sysUser?: User;
  createDate?: Date;
  createDateShow?: any;
  updateDate?: Date;
  isSelected?: boolean;
  status?: number;
  checkMsg?: string;
}

export  enum  CheckStatus {
  UNREVIEW = 0,
  UNPASS = 1,
  PASS = 2,
  UNDERSHELF = 3
}
export  const CheckStatusMap = ['待审核', '未通过', '已通过', '已废弃']
