import { NgModule } from '@angular/core';
import { ShareModule } from './share.module';

import { HomeRoutingModule } from '../router/home';

import { HomePageComponent } from '../components/homePage/homePage';
import { CompanyModule } from './company.module';

@NgModule({
  imports: [
    ShareModule,
    HomeRoutingModule,
    CompanyModule
  ],
  declarations: [
    HomePageComponent,
  ],
  providers: [
  ]
})
export class HomeModule {}
