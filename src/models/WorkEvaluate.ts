export class WorkEvaluate {
  comment?: string;
  companyId?: number;
  createDate?: Date;
  evaluationUserId?: number;
  id?: number;
  imgs?: string;
  num1: number;
  num2?: number;
  num3?: number;
  num4?: number;
  num5?: number;
  projectId?: number;
  status?: number;
  type?: number;
  userId?: number;
}
