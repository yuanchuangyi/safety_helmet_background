import { Component, Input, OnInit } from '@angular/core';
import { environment } from '../../../src/environments/environment';
import { Company } from '../../../src/models/Company';
import { WorkProject } from '../../../src/models/WorkProject';

@Component({
  selector: 'app-check-box',
  templateUrl: './checkBox.html',
  styleUrls: ['./checkBox.css']
})
export class CheckBoxComponent implements OnInit {

  @Input()
  resource: any;
  @Input()
  url: string;
  @Input()
  isArticle: boolean;
  environment = environment;

  ngOnInit() {
      console.log(this.resource);
  }

  onSelectToggle() {
    this.resource.isSelected = !this.resource.isSelected;
  }
}
