import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ShareModule } from './share.module';

import { VideoMonitorComponent } from '../components/videoMonitor/videoMonitor';

import { VideomonitorRoutingModule } from '../router/videomonitor';

@NgModule({
  imports: [
    BrowserModule,
    ShareModule,
    VideomonitorRoutingModule
  ],
  declarations: [
    VideoMonitorComponent
  ],
  providers: [

  ],
  entryComponents: [
    VideoMonitorComponent
  ],
  exports: [

  ]
})
export class VideoMonitorModule { }
