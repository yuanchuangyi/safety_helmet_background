import { Component } from '@angular/core';
import { CheckStatus,  CheckStatusMap} from  '../../../src/models/Company';

@Component({
  selector: 'app-status-nav',
  templateUrl: './statusNav.html',
  styleUrls: ['./statusNav.css']
})
export class StatusNavComponent {

  CheckStatus = CheckStatus;
  CheckStatusMap = CheckStatusMap;

  constructor(
  ) {
  }

}
