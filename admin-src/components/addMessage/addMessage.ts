import { AfterViewChecked, Component, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
// import { AuthService } from '../../../src/service/AuthService';
import { ActivatedRoute, Router } from '@angular/router';

import { MessageService } from '../../service/MessageService';
import { environment } from '../../../src/environments/environment';
import { WorkMsg } from '../../../src/models/WorkMsg';
// import { UploaderOptions, UploadFile, UploadInput, UploadOutput } from 'ngx-uploader';
// import { MatSnackBar } from '@angular/material';
import { EditorComponent } from '../editor/editor';

@Component({
  templateUrl: './addMessage.html',
  styleUrls: ['./addMessage.css']
})
export class AddMessageComponent implements AfterViewChecked, OnInit {
  @ViewChild('subForm')
  subForm: NgForm;
  isUpload: boolean;
  environment = environment;
  message: WorkMsg;
  @ViewChild(EditorComponent)
  editor: EditorComponent;
  submitted: boolean;
  static ErrorMessages = {
    title: {
      required: '专题名称不能为空',
    },
    description: {
      required: '专题简介不能为空',
    }
  };
  messages = {
    title: '',
    description: '',

  };
  constructor(
    // private authService: AuthService,
    private messageService: MessageService,
    private route: ActivatedRoute,
    private router: Router,
    // private bar: MatSnackBar,
  ) {}
  ngAfterViewChecked() {
    if (this.subForm) {
      this.subForm.valueChanges.subscribe(data => this.checkValidate());
    }
  }
  ngOnInit() {
    this.message = {};
    // this.options = { concurrency: 1 };

    this.route.data.subscribe((res: {message: WorkMsg}) => {
      if (res.message) {
        this.message = res.message;
      }
    });
  }
  // onUploadOutput(output: UploadOutput): void {
  //   if ( output.type === 'allAddedToQueue') {
  //     const event: UploadInput = {
  //       type: 'uploadAll',
  //       url: environment.apiUrl + 'api/fileupload/add',
  //       method: 'POST',
  //       fieldName: 'attach',
  //     };
  //     this.uploadInput.emit(event);
  //   } else if ( output.type === 'done' && typeof output.file !== 'undefined' ) {
  //     this.message.headerImg = output.file.response.res;
  //     this.isUpload = true;
  //   }
  // }
  checkValidate(isSubmit?: boolean) {
    const form = this.subForm;
    for ( const field in AddMessageComponent.ErrorMessages ) {
      const control = form.controls[field];
      if (control && control.valid) {
        this.messages[field] = '';
      }
      if (control && (control.dirty || isSubmit) && !control.valid) {
        const messages = AddMessageComponent.ErrorMessages[field];
        if (control.errors) {
          this.messages[field] = messages[Object.keys(control.errors)[0]];
        }
      }
    }
  }
  onSave() {
    this.checkValidate(true);
    if (this.subForm.invalid) {
      return false;
    }
    this.message.content = this.editor.getContent();
    this.messageService.insertMessage(this.message).subscribe((res) => {
      if (res.success) {
        this.submitted = true;
        this.router.navigate(['/messageManage']);
      } else {
        // this.bar.open(res.errorMsg);
      }
    });
  }
}
