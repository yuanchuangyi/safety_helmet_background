import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ShareModule } from './share.module';
import { CompanyService } from '../service/CompanyService';
import { CompanyResolveService } from '../service/CompanyResolveService';

@NgModule({
  imports: [BrowserModule, ShareModule],
  declarations: [

  ],
  providers: [
    CompanyService,
    CompanyResolveService
  ]
})
export class HomeModule { }
