import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ShareModule } from './share.module';
import { CompanyRoutingModule } from '../router/company';
import { CompanyProveComponent } from '../components/companyProve/companyProve';

import { CompanyService } from '../service/CompanyService';

@NgModule({
  imports: [
    BrowserModule,
    ShareModule,
    CompanyRoutingModule
  ],
  declarations: [
    CompanyProveComponent
  ],
  exports: [
    CompanyProveComponent
  ],
  providers: [
    CompanyService
  ],
  entryComponents: [

  ]
})
export class CompanyModule { }
