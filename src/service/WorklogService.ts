
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import {map, switchMapTo} from 'rxjs/operators';
import { environment } from '../environments/environment';
import { WorkLog } from '../models/WorkLog';

@Injectable()
export class WorklogService {
  private baseUrl = environment.apiUrl + 'api/workprojectlog';
  constructor(
    private http: HttpClient
  ) {
  }
  getlog( current: number = 0, pageSize: number = environment.pageSize, options: {jobId?: number, projectId?: number }): Observable<{res: WorkLog[], success: boolean, total: number}> {
    const paramsObj: any = {
      pageSize: String(pageSize),
      page: String(current)
    };
    if (options.jobId) paramsObj.jobId = options.jobId;
    if (options.projectId) paramsObj.projectId = options.projectId;
    const params = new HttpParams({ fromObject: paramsObj });
    return this.http.get<{res: WorkLog[], success: boolean, total: number}>(`${this.baseUrl}/list`,{params: params});
  }

  deleteLog(id): Observable<{res: WorkLog, success: boolean}> {
    return this.http.delete<{res: WorkLog, success: boolean}>(`${this.baseUrl}/del/${id}`);
  }

  insertLog(worklog: WorkLog): Observable<{res: WorkLog, success: boolean}> {
    return this.http.post<{res: WorkLog, success: boolean}>(`${this.baseUrl}/insert`, worklog);
  }

  updateLog(worklog: WorkLog): Observable<{res: WorkLog, success: boolean}> {
    return this.http.patch<{res: WorkLog, success: boolean}>(`${this.baseUrl}/update`, worklog);
  }
}
