import { Component, OnInit } from '@angular/core';

import { User } from '../../../src/models/User';
import { environment } from '../../../src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';

import { ConfirmDialogComponent } from '../../../src/components/confirmDialog/confirmDialog';
import { MatDialog, MatSnackBar } from '@angular/material';
import { WorkerTopService} from '../../service/WorkerTopService';
import {CheckStatus} from '../../../src/models/Company';
import {tap} from 'rxjs/operators';
import {adjustList, trackById} from '../../../src/others/utils';

@Component({
  templateUrl: './workerTop.html',
  styleUrls: ['./workerTop.css']
})
export class WorkerTopComponent implements OnInit {
  status: CheckStatus;
  workerTops: User[];
  workerTop: User;
  total: number;
  currentPage = 1;
  pageSize = environment.adminPageSize;
  environment = environment;
  trackById = trackById;
  constructor(
    private route: ActivatedRoute,
    private workerTopService: WorkerTopService,
    private dialog: MatDialog,
    private bar: MatSnackBar,
    private router: Router,
  ) {}

  ngOnInit() {
    this.route.data.subscribe((data: { result: { total: number, res: User[] } }) => {
      this.workerTops = data.result.res;
      this.total = data.result.total;
    });
    this.workerTop = {};

  }


  onPageChanged(page: number) {
    this.update(page).subscribe();
  }

  update(page: number) {
    // const options: any = { status };
    return this.workerTopService.getAllWorkertops(page, this.pageSize).pipe(tap((res) => {
      if (res.success) {
        this.currentPage = page;
        this.total = res.total;
        // this.companys = res.res;
        this.workerTops = adjustList(res.res, 4);
      } else {
        this.bar.open(res.errorMsg);
      }
    }));
  }

  onLock( user: User) {
    user.status = 1;
    this.workerTopService.updateStatus(user).subscribe((res) => {
      if (res.success) {
        this.update(this.currentPage);
      } else {
        console.log('ERROR');
      }
    })
  }

  onOpen( user: User) {
    user.status = 0;
    this.workerTopService.updateStatus(user).subscribe((res) => {
      if (res.success) {
        this.update(this.currentPage);
      } else {
        console.log('ERROR');
      }
    })
  }
}
