import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { CheckStatus } from '../../../src/models/Company';
import { ConfirmDialogComponent } from '../../../src/components/confirmDialog/confirmDialog';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Video } from '../../../src/models/Video';
import { VideoService } from '../../service/VideoService';

@Component({
  templateUrl: 'videoDetail.html',
  styleUrls: ['videoDetail.css']
})
export class VideoDetailComponent implements OnInit {
  videoId: number;
  video: Video;
  CheckStatus = CheckStatus;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private videoService: VideoService,
              private bar: MatSnackBar,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.videoId = +this.route.snapshot.paramMap.get('id');
    this.video = new Video();
    this.videoService.getOneVideo(this.videoId).subscribe((res) => {
      if (res.success) {
        this.video = res.res;
      }
    })
  }

  onReview(status: CheckStatus) {
    this.video.status = status;
    this.videoService.reviewVideo([{id: this.videoId, status, checkMsg: this.video.checkMsg}]).subscribe((res) => {
      if (res.success) {
        this.onReturn(status);
      } else {
        this.bar.open(res.errorMsg);
      }
    })
  }

  onDelete() {
    this.videoService.deleteVideo(this.videoId).subscribe((res) => {
      if (res.success) {
        alert('删除成功');
        // this.onReturn(this.company.status);
      } else {
        this.bar.open(res.errorMsg);
      }
    })
  }

  onReturn(status: CheckStatus) {
    let path: string;
    if (status === CheckStatus.UNPASS) {
      path = '../../unpassed';
    } else if (status === CheckStatus.UNDERSHELF) {
      path = '../../undershelf';
    } else if (status === CheckStatus.UNREVIEW) {
      path = '../../unreview';
    } else {
      path = '../../published';
    }
    this.router.navigate([path], { relativeTo: this.route });
  }
}
