import { Component, OnInit, Inject, AfterViewChecked, ViewChild } from '@angular/core';
import {FormControl, NgForm} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDatepickerInputEvent } from '@angular/material';
import { Moment } from 'moment';
import * as moment from 'moment';
import { User } from '../../models/User';
import { UserRealinfo } from '../../models/UserRealinfo';
import { UserService } from '../../service/UserService';


@Component({
  templateUrl: './workerAdd.html',
  styleUrls: ['./workerAdd.css']
})
export class  WorkerAddComponent implements OnInit, AfterViewChecked {

  messages = {
    username: '',
    age: '',
    sex: '',
    personNum: '',
    address: '',
    phone: '',
    email: ''
  };
  userinfo: UserRealinfo;
  worker: User;
  constructor(
    private dialogRef: MatDialogRef<WorkerAddComponent>,
    private userService: UserService,
    @Inject(MAT_DIALOG_DATA) public data: {worker: User}
  ) {
    this.worker = data.worker || new User();
    this.worker.beginDateShow = new FormControl(moment(this.worker.beginDate));
    this.worker.beginDate = moment(this.worker.beginDate).toDate();
    this.worker.endDateShow = new FormControl(moment(this.worker.endDate));
    this.worker.endDate = moment(this.worker.endDate).toDate();
  }

  @ViewChild('workerForm')
  workerForm: NgForm;

  ngAfterViewChecked() {
    if (this.workerForm) {
      this.workerForm.valueChanges.subscribe(data => this.checkValidate());
    }
  }
  ngOnInit() {

  }

  onSubmit() {
    this.checkValidate(true);
    if (this.workerForm.invalid) {
      // 触发mat的error
      this.workerForm.onSubmit(null);
      return false;
    }
    if (this.worker.id !== undefined) {
      this.userService.update(this.worker).subscribe((res) => {
        this.dialogRef.close(res.res);
      });
    } else {
      this.userService.register(this.worker, '1234').subscribe((res) => {
        this.dialogRef.close(res.res);
      });
    }

  }
  onSelectbeginDate( event: MatDatepickerInputEvent<Moment>) {
    this.worker.beginDate = event.value.toDate();
    this.worker.beginDateShow = new FormControl(event.value);
  }
  onSelectendDate( event: MatDatepickerInputEvent<Moment>) {
    this.worker.endDate = event.value.toDate();
    this.worker.endDateShow = new FormControl(event.value);
  }

  checkValidate(isSubmit?: boolean) {
    const form = this.workerForm;

    for (const field in WorkerAddComponent.ErrorMessages) {

      const control = form.controls[field];

      if (control && control.valid) {
        this.messages[field] = '';
      }
      if (control && (control.dirty || isSubmit) && !control.valid) {
        const messages = WorkerAddComponent.ErrorMessages[field];
        if (control.errors) {
          this.messages[field] = messages[Object.keys(control.errors)[0]];
        }
      }

    }
  }

  static ErrorMessages = {
    username: {
      required: '姓名不能为空'
    },
    age: {
      required: '年龄不能为空'
    },
    sex: {
      required: '性别不能为空'
    },
    personNum: {
      required: '身份证号不能为空'
    },
    address: {
      required: '籍贯不能为空'
    },
    phone: {
      pattern: '手机号格式不正确',
      required: '手机号不能为空'
    },
    email: {
      pattern: '邮箱格式不正确',
      required: '邮箱不能为空'
    }
  };
}
