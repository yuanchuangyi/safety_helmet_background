import { Component, OnInit, AfterViewChecked, Input } from '@angular/core';

import { FileService } from '../../service/FileService';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.html',
  styleUrls: ['./editor.css'],
})
export class EditorComponent implements OnInit {

  public options: Object = {
    language: 'zh_cn',
    imageUploadURL: environment.apiUrl + 'api/fileupload/upload',
    videoUploadURL: environment.apiUrl + 'api/fileupload/upload',
    imageUploadParam: 'attach',
    videoUploadParam: 'attach',
    fontFamily: {
      'Microsoft YaHei, sans-serif': '微软雅黑',
      'SimHei, sans-serif': '黑体',
      'SimSun, sans-serif': '宋体',
      'KaiTi, sans-serif': '楷体',
      'Arial': 'Arial',
      'Times New Roman,Times, sans-serif': 'Times New Roman'
    },
    fontFamilySelection: true,
    help: false,
    height: 250,
    width: 550,
    toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', '|',
      'fontFamily', 'fontSize', 'color', '|',
      'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '|',
      'insertLink', 'insertImage', 'insertVideo', 'insertTable', '|',
      'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|',
      'print', 'spellChecker', 'help', 'html', '|', 'undo', 'redo'],
    events : {

    }

  };

  @Input()
  content: string;
  untouched = true;



  constructor(
    private fileService: FileService
  ) {

  }


  ngOnInit() {
  }


  onChange() {
    this.untouched = false;
  }

  getContent() {
    return this.content;
  }


}
