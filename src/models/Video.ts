import { User } from './User';

export class Video {
  checkMsg?: string;
  clickNum?: number;
  commentNum?: number;
  content?: string;
  createDate?: Date;
  createUserId?: number;
  description?: string;
  favorite?: number;
  id?: number;
  keyWords?: string;
  likes?: number;
  otherProperty?: string;
  shareNum?: number;
  status?: number;
  title?: string;
  type?: number;
  updateDate?: Date;
  updateUserid?: number;
  url?: string;
  userId?: number;
  user?: User;
}
