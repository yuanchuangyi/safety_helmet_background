import { Component, ViewChild, AfterViewChecked } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { NgForm } from '@angular/forms';
import { tap, filter, switchMapTo } from 'rxjs/operators';
import { interval } from 'rxjs';
import { AuthService } from '../../service/AuthService';
import { UserService } from '../../service/UserService';
import { User } from '../../models/User';

@Component({
  selector: 'app-register',
  templateUrl: 'register.html',
  styleUrls: ['register.css'],
})
export class RegisterComponent implements AfterViewChecked {

  user: User;
  userid: number;
  loading: Boolean = false;
  messages = {
    username: '',
    password: '',
    confirmPassword: '',
    phone: '',
    keyWord: ''
  };
  timeNum = 0;
  @ViewChild('registerForm')
  registerForm: NgForm;

  ngAfterViewChecked() {
    // Angular的“单向数据流”规则禁止在一个视图已经被组合好之后再更新视图.而此时已组合好
    if (this.registerForm) {
      this.registerForm.valueChanges.subscribe(data => this.checkValidate());
    }
  }

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private router: Router
  ) {
    this.user = new User();

  }

  register() {
    this.user.workerType = '2';
    this.checkValidate(true);
    if (this.registerForm.invalid) {
      return false;
    }
    this.userService.checkPhone(this.user.phone).pipe(
      tap((res) => {
        if (!res.success) {
          this.messages.phone = RegisterComponent.ErrorMessages.phone.hasExist;
        }
      }),
      // 如果success为false过滤掉。不执行
      filter(res => res.success),
      switchMapTo(this.userService.register(this.user, this.user.keyWord)))
      .subscribe((res: {success: boolean, res: User}) => {
        if (res.success) {
          this.authService.user = res.res;
          this.authService.userSource.next(res.res);
          // const redirect: any[] = this.authService.redirectUrl ?
          //   [this.authService.redirectUrl]
          //   : ['/realnameProve'];
          // const navigationExtras: NavigationExtras = {
          //   queryParamsHandling: 'merge',
          //   preserveFragment: true ,
          // };
            this.userid = res.res.id;
            // this.router.navigate(['/realnameProve'], {queryParams: {'userid': this.userid}});
          this.router.navigate(['/login']);
        } else {
          this.messages.keyWord = RegisterComponent.ErrorMessages.keyWord.error;
        }
      });
  }


  checkValidate(isSubmit?: boolean) {
    const form = this.registerForm;

    for (const field in RegisterComponent.ErrorMessages) {

      const control = form.controls[field];

      if (control && control.valid) {
        this.messages[field] = '';
      }
      if (control && (control.dirty || isSubmit) && !control.valid) {
        const messages = RegisterComponent.ErrorMessages[field];
        if (control.errors) {
          this.messages[field] = messages[Object.keys(control.errors)[0]];
        }
      }

    }
  }
  onGetPhonecode(phone) {
    this.messages.keyWord = '';
    const phoneControl = this.registerForm.controls['phone'];
    if (phoneControl.invalid) {
      this.messages.phone = RegisterComponent.ErrorMessages.phone[Object.keys(phoneControl.errors)[0]];
      return false;
    }

    this.userService.bindPhone(phone).subscribe((res) => {
      if (!res.success) {
        this.messages.phone = res.errorMsg;
      } else {
        const sub = interval(1000).subscribe((x) => {
          this.timeNum = 60 - x;
          if (this.timeNum <= 0) {
            this.timeNum = 0;
            sub.unsubscribe();
          }
        });
      }
    })

  }


  static ErrorMessages = {
    username: {
      required: '用户名不能为空',
      hasExist: '用户名已存在',
      pattern: '以字母开头，由4-20个字母、数字和"_"组成'
    },
    password: {
      required: '密码不能为空',
      pattern: '长度为6-12位，必须由数字，字母共同组成'
    },
    confirmPassword: {
      required: '确认密码不能为空',
      pattern: '两次密码不一样'
    },
    phone: {
      required: '手机号不能为空',
      hasExist: '手机号已存在',
      pattern: '手机号格式不正确'
    },
    keyWord: {
      required: '验证码不能为空',
      error: '验证码错误11'
    },
  };

}
