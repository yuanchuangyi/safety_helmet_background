
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { UserRealinfo } from '../../src/models/UserRealinfo';
import { RealinfoService } from './RealinfoService';
import { environment } from '../../src/environments/environment';

import { CheckStatus } from '../../src/models/Company';

@Injectable()
export class RealinfoResolveService implements Resolve<{ res: UserRealinfo[], total: number}> {

  constructor(private realinfoService: RealinfoService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{ res: UserRealinfo[], total: number}>  {

    let status: CheckStatus;
    if (state.url.indexOf('published') > 0) {
      status = CheckStatus.PASS;
    } else if (state.url.indexOf('unpassed') > 0) {
      status = CheckStatus.UNPASS;
    } else if (state.url.indexOf('undershelf') > 0) {
      status = CheckStatus.UNDERSHELF;
    } else  {
      status = CheckStatus.UNREVIEW;
    }
    return this.realinfoService.getAllRealinfos(1, environment.adminPageSize, { status }).pipe(map((res) => {
      if (res.success) {
        return  res;
      } else {
        console.error(res.errorMsg);
      }
    }));
  }
}

