import { Component, OnInit, Inject, AfterViewChecked, ViewChild } from '@angular/core';
import {FormControl, NgForm} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDatepickerInputEvent } from '@angular/material';

import { User } from '../../models/User';
import { JobUser } from '../../models/JobUser';
import { UserRealinfo } from '../../models/UserRealinfo';
import { UserinfoService } from '../../service/UserinfoService';

@Component({
  templateUrl: './workerRealinfo.html',
  styleUrls: ['./workerRealinfo.css']
})
export class WorkerRealinfoComponent implements OnInit {
  worker: JobUser;
  workerRealinfo: UserRealinfo;
  haveRealinfo: boolean;
  constructor(
    private dialogRef: MatDialogRef<WorkerRealinfoComponent>,
    private  userinfoService: UserinfoService,
    @Inject(MAT_DIALOG_DATA) public data: {worker: JobUser}
  ) {
    this.worker = data.worker;
  }
  @ViewChild('workerForm')
  workerForm: NgForm;


  ngOnInit() {
    // this.haveRealinfo = true;
    this.workerRealinfo = new UserRealinfo();
      this.userinfoService.getoneInfo(this.worker.sysUser.id).subscribe((res) => {
        if (res.res) {
          this.workerRealinfo = res.res;
          this.haveRealinfo = true;
        } else {
          this.haveRealinfo = false;
        }
      })
  }

  onSubmit() {
    this.dialogRef.close();
  }





}
