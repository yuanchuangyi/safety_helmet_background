import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuardService } from '../service/AuthGuardService';
import { CompanyProveComponent } from '../components/companyProve/companyProve';

const routes: Routes = [
  {
    path: 'companyProve',
    component: CompanyProveComponent,
    canActivate: [AuthGuardService],
    resolve: {

    }
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ],


})
export class CompanyRoutingModule {}
