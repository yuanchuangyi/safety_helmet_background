
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import {map, switchMapTo} from 'rxjs/operators';
import { environment } from '../environments/environment';
import { Workjob } from '../models/Workjob';

@Injectable()
export class WorkjobService {
  private baseUrl = environment.apiUrl + 'api/workprojectjob';
  constructor(
    private http: HttpClient
  ) {
  }
  getalljob( current: number = 0, pageSize: number = environment.pageSize, options: {  projectId?: number, status?: number}): Observable<{res: Workjob[], success: boolean, total: number}> {
    const paramsObj: any = {
      pageSize: String(pageSize),
      page: String(current)
    };
    if (options.projectId) paramsObj.projectId = options.projectId;
    if (options.status) paramsObj.status = options.status;
    const params = new HttpParams({ fromObject: paramsObj });
    return this.http.get<{res: Workjob[], success: boolean, total: number}>(`${this.baseUrl}/alllist`,{params: params});
  }

  getmyjob( current: number = 0, pageSize: number = environment.pageSize, userId: string): Observable<{res: Workjob[], success: boolean, total: number}> {
    const paramsObj: any = {
      createUserId: userId,
      pageSize: String(pageSize),
      page: String(current),
    };
    const params = new HttpParams({ fromObject: paramsObj});
    return this.http.get<{res: Workjob[], success: boolean, total: number}>(`${this.baseUrl}/mylist`,{params: params});
  }
  deleteJob(id): Observable<{res: Workjob, success: boolean}> {
    return this.http.delete<{res: Workjob, success: boolean}>(`${this.baseUrl}/del/${id}`);
  }

  insertJob(workjob: Workjob): Observable<{res: Workjob, success: boolean}> {
    return this.http.post<{res: Workjob, success: boolean}>(`${this.baseUrl}/insert`, workjob);
  }

  updateJob(workjob: Workjob): Observable<{res: Workjob, success: boolean}> {
    return this.http.patch<{res: Workjob, success: boolean}>(`${this.baseUrl}/update`, workjob);
  }
}
