import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Md5 } from 'ts-md5';
import { AuthService } from '../../service/AuthService';
import { User } from '../../models/User';

declare var RL_YTX: any;

@Component({
  templateUrl: './workChat.html',
  styleUrls: ['./workChat.css'],
})
export class WorkChatComponent implements OnInit {
  userInfo: User;
  constructor(
    private authService: AuthService
  ) {
  }

  ngOnInit() {
    this.userInfo = new User();
    this.authService.getCurrent().subscribe((res) => {
      this.userInfo = res.res;
      const resp = RL_YTX.init('8aaf0708659e8e420165a3fedae3040c');
      console.log('resp.code', resp.code);
      if (170002 === resp.code) {

      } else if (174001 === resp.code) {

      } else if (200 === resp.code) {
        const unsupport = resp.unsupport;
        this.success();
      }
    })

  }

  success() {
    const loginBuilder = new RL_YTX.LoginBuilder();
    loginBuilder.setType(1);
    loginBuilder.setUserName(this.userInfo.username);
    loginBuilder.setPwd();
    // loginBuilder.setPwd(this.userInfo.pass);
    const timeType = this.getTimeStamp();
    const need = '8aaf0708659e8e420165a3fedae3040c' + this.userInfo.username + timeType + '74023c89c986a93eca087b736f325d9c';
    const md5 = Md5.hashStr(need).toString();
    loginBuilder.setSig(md5);
    loginBuilder.setTimestamp(timeType);
    const _this = this;
    RL_YTX.login(loginBuilder, function (obj) {
      console.log('错误码：', obj);
    });
  }
  getTimeStamp() {
    const now = new Date();
    const timestamp = now.getFullYear() + '' + ((now.getMonth() + 1) >= 10 ? '' + (now.getMonth() + 1) : '0' + (now.getMonth() + 1))
      + (now.getDate() >= 10 ? now.getDate() : '0' + now.getDate()) +
      (now.getHours() >= 10 ? now.getHours() : '0' + now.getHours()) + (now.getMinutes() >= 10 ? now.getMinutes() : '0' + now.getMinutes())
      + (now.getSeconds() >= 10 ? now.getSeconds() : '0' + now.getSeconds());
    return timestamp;
  }

}





