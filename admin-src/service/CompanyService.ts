
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import {map, switchMapTo} from 'rxjs/operators';
import { environment } from '../environments/environment';

import { User } from '../../src/models/User';
import { Company } from '../../src/models/Company';
import { CheckStatus } from '../../src/models/Company';

@Injectable()
export class CompanyService {
  private baseUrl = environment.apiUrl + 'api';


  constructor(
    private http: HttpClient
  ) {
  }

  getAllCompanys(current: number = 1, pageSize: number = environment.pageSize, options: { status?: CheckStatus } = { status: CheckStatus.UNREVIEW })
    : Observable<{ success: boolean, errorMsg: string, res: Company[], total: number }> {
    const paramsObj: any = {
      page: current,
      pageSize: pageSize,
      status: String(options.status),

    };
    const params = new HttpParams({ fromObject: paramsObj });
    return this.http.get<{ success: boolean, errorMsg: string, res: Company[], total: number }>(`${this.baseUrl}/company/alllist`, { params });
  }

  reviewCompanyStatus(companys: Array<Company>): Observable<{success: boolean; res: number, errorMsg: string}> {
    return this.http.patch<{ success: boolean, res: number, errorMsg: string}>(`${this.baseUrl}/company/review`, companys);
  }

  getoneComapny(id): Observable<{res: Company; success: boolean}> {
    return this.http.get<{ res: Company, success: boolean}>(`${this.baseUrl}/company/getone/${id}`);
  }

  deleteCompany(id): Observable<{res: Company; success: boolean, errorMsg: string}> {
    return this.http.delete<{ res: Company, success: boolean, errorMsg: string}>(`${this.baseUrl}/company/del/${id}`);
  }
}
