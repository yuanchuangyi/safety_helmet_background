import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VideoManageComponent } from '../components/videoManage/videoManage';
import { VideoDetailComponent } from '../components/videoDetail/videoDetail';
import { AuthGuardService } from '../service/AuthGuardService';
import { VideoResolveService } from '../service/VideoResolveService';


const routes: Routes = [
  {
    path: 'videoManage',
    canActivate: [AuthGuardService],
    children: [
      {
        path: '',
        redirectTo: 'unreview',
        pathMatch: 'full'
      },
      {
        path: 'unreview',
        component: VideoManageComponent,
        resolve: {
          result: VideoResolveService
        }
      },
      {
        path: 'published',
        component:  VideoManageComponent,
        resolve: {
          result: VideoResolveService
        }
      },
      {
        path: 'unpassed',
        component:  VideoManageComponent,
        resolve: {
          result: VideoResolveService
        }
      },
      {
        path: 'undershelf',
        component:  VideoManageComponent,
        resolve: {
          result: VideoResolveService
        }
      },
      {
        path: 'videoDetail/:id',
        component: VideoDetailComponent,
        resolve: {
          result: VideoResolveService
        }
      }
    ]
  }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class VideoRoutingModule {}
