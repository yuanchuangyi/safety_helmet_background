import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuardService } from '../service/AuthGuardService';
import { WorkJobComponent } from '../components/workJob/workJob';
// import { WorkjobResolveService } from '../service/WorkjobResolveService';

const routes: Routes = [
  {
    path: 'workjob',
    canActivate: [AuthGuardService],
    component: WorkJobComponent,
    resolve: {

    }
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ],


})
export class WorkjobRoutingModule {}
