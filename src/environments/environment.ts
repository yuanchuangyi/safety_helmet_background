// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiUrl: 'http://file.sxycy.cn:84/',
  imgUrl: 'http://file.sxycy.cn:8888/',
  pageSize: 5,
  adminPageSize: 6,
};

export const myFormats = {
  parse: {
    dateInput: 'LL'
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'YYYY-MM',
    dateA11yLabel: 'DD',
    monthYearA11yLabel: 'YYYY-MM',
  }
};
