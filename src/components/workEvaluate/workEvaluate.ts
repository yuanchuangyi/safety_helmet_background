import { Component, OnInit, ViewChild, Inject, } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {MatDialogRef, MAT_DIALOG_DATA, MatDatepickerInputEvent, MatDialog} from '@angular/material';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { WorkProject } from '../../models/WorkProject';
import { JobUser } from '../../models/JobUser';
import { WorkEvaluate } from '../../models/WorkEvaluate';

import { WorkjobService } from '../../service/WorkjobService';
import { WorkEvaluateService } from '../../service/WorkEvaluateService';
import { CompanyService } from '../../service/CompanyService';
import { AuthService } from '../../service/AuthService';

import { EvaluateAddComponent} from '../evaluateAdd/evaluateAdd';
import { MsgAddComponent } from '../msgAdd/msgAdd';
import {ConfirmDialogComponent} from '../confirmDialog/confirmDialog';


@Component({
  templateUrl: './workEvaluate.html',
  styleUrls: ['./workEvaluate.css']
})
export class WorkEvaluateComponent implements OnInit {

  selection: SelectionModel<WorkEvaluate>;
  workEvaluations: WorkEvaluate[];
  total: number;
  subscriptions: Subscription[] = [];
  pageSize = environment.pageSize;
  page = 0;
  workplace: WorkProject;
  projectId: number;
  companyId: number;
  evaluationUserId: number;
  worker: JobUser;
  comment: boolean;
  workerTop: boolean = false;
  type: number = 1;
  cols: string[] = ['select', 'comment', 'type', 'num'];
  constructor(
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<WorkEvaluateComponent>,
    private route: ActivatedRoute,
    private router: Router,
    private workjobService: WorkjobService,
    private workEvaluateService: WorkEvaluateService,
    private companyService: CompanyService,
    private authService: AuthService,
    @Inject(MAT_DIALOG_DATA) public data: { worker: JobUser, projectId: number}
  ) {
    this.worker = data.worker;
    this.projectId = data.projectId;
  }

  ngOnInit() {
    this.workerTop = false;
    this.comment = true;
    this.authService.getCurrent().subscribe((res) => {
      this.evaluationUserId = res.res.id;
    })
    this.companyService.getCompany().subscribe((res) => {
      if (res.success) {
        this.companyId = res.res[0].id;
      }
    })
    this.update(this.page, this.type).subscribe();
    const initialSelection = [];
    const allowMultiSelect = true;
    this.selection = new SelectionModel<WorkEvaluate>(allowMultiSelect, initialSelection);

  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.workEvaluations.length;
    return numSelected === numRows;
  }

  allToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.workEvaluations.forEach(row => this.selection.select(row));
  }

  update(page: number, type: number) {
    const companyId = this.companyId;
    const projectId = this.projectId;
    const evaluatioinUserId = this.authService.user.id;
    const userId = this.worker.id;
    const options: any = { companyId, projectId, evaluatioinUserId, userId };
    return this.workEvaluateService.getEvaluate(page, this.pageSize, type, options).pipe(tap((res) => {
      if (res.success) {
        this.page = page;
        this.total = res.res.length;
        this.workEvaluations = res.res;
        if (this.total !== 0) {
          this.comment = false;
        } else {
          this.comment = true;
        }
        // this.workEvaluations.forEach(value => {
        //   if (value.evaluationUserId === this.evaluationUserId) {
        //     this.comment = false;
        //   } else {
        //     this.comment = true;
        //   }
        // })
      }
    }));

  }
  onTypeChange(type: number) {
    this.type = type;
    if (this.type === 2) {
      this.workerTop = true;
    } else {
      this.workerTop = false;
    }
    this.update(this.page, type).subscribe();
  }

  onPageChanged(page: number) {
    this.page = page;
    this.update(page, this.type).subscribe();
  }

  trackById(index: number, item: any) {
    return item.id;
  }

  onOpenAdd() {
      const dialogRef = this.dialog.open(EvaluateAddComponent, {
        width: '500px',
        height: '500px',
        data: {
          companyId: this.companyId,
          projectId: this.projectId,
          userId: this.worker.id
        }
      });
      dialogRef.afterClosed().subscribe((res) => {
        this.update(this.page, this.type).subscribe();
      });
  }

  onEdit() {
    const dialogRef = this.dialog.open(EvaluateAddComponent, {
      width: '500px',
      height: '500px',
      data: {
        companyId: this.companyId,
        projectId: this.projectId,
        userId: this.worker.id
      }
    });
    dialogRef.afterClosed().subscribe((res) => {
      this.update(this.page, this.type).subscribe();
    });
  }
  // onDelete(id) {
  //     const dialogRef = this.dialog.open(ConfirmDialogComponent, {
  //       width: '400px',
  //       height: '200px',
  //       data: {message: '确认删除？'}
  //     });
  //     dialogRef.afterClosed().subscribe((result) => {
  //       if (result) {
  //         this.workEvaluateService.deleteEvaluate(id).subscribe((res) => {
  //           if (res.success) {
  //             this.update(this.page, this.type);
  //           }
  //         });
  //       }
  //     });
  // }
}
