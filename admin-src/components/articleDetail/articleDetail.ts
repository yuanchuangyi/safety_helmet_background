import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Article } from '../../../src/models/Article';
import { ArticleService } from '../../service/ArticleService';
import { CheckStatus } from '../../../src/models/Company';
import { ConfirmDialogComponent } from '../../../src/components/confirmDialog/confirmDialog';
import { MatDialog, MatSnackBar } from '@angular/material';
import { User } from '../../../src/models/User';

@Component({
  templateUrl: 'articleDetail.html',
  styleUrls: ['articleDetail.css']
})
export class ArticleDetailComponent implements OnInit {
  articleId: number;
  article: Article;
  CheckStatus = CheckStatus;
  user: User;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private  articleService: ArticleService,
              private bar: MatSnackBar,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.articleId = +this.route.snapshot.paramMap.get('id');
    this.article = new Article();
    this.user = new User();
    this.articleService.getOneArticle(this.articleId).subscribe((res) => {
      if (res.success) {
        this.article = res.res;
        this.user = res.user;
      }
    })
  }

  onReview(status: CheckStatus) {
    this.article.status = status;
    this.articleService.reviewArticle([{id: this.articleId, status, checkMsg: this.article.checkMsg}]).subscribe((res) => {
      if (res.success) {
        this.onReturn(status);
      } else {
        this.bar.open(res.errorMsg);
      }
    })
  }

  onDelete() {
    this.articleService.deleteArticle(this.articleId).subscribe((res) => {
      if (res.success) {
        alert('删除成功');
        // this.onReturn(this.company.status);
      } else {
        this.bar.open(res.errorMsg);
      }
    })
  }

  onReturn(status: CheckStatus) {
    let path: string;
    if (status === CheckStatus.UNPASS) {
      path = '../../unpassed';
    } else if (status === CheckStatus.UNDERSHELF) {
      path = '../../undershelf';
    } else if (status === CheckStatus.UNREVIEW) {
      path = '../../unreview';
    } else {
      path = '../../published';
    }
    this.router.navigate([path], { relativeTo: this.route });
  }


}
