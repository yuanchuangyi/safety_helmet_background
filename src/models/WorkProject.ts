import { Company } from './Company';
export class WorkProject {
  companyID?: number;
  companyName_?: string;
  createUserId?: number;
  description?: string;
  id?: number;
  imgUrls?: string;
  latitude?: number;
  longitude?: number;
  nowWorkers?: number;
  projectAddress?: string;
  projectCode?: string;
  projectName?: string;
  projectPhone?: string;
  totalWorkers?: number;
  status?: number;
  sysCompany?: Company;
  endDate?: Date;
  endDateShow?: any;
  startDate?: Date;
  startDateShow?: any;
  createDate?: Date;
  updateDate?: Date;
  workers?: any[];
  showWorkers?: boolean = true;
  haschild?: boolean = true;
  isSelected?: boolean;
  checkMsg?: string;
}
