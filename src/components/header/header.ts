import {Component, OnInit, Output, EventEmitter, Input, HostListener} from '@angular/core';

import { AuthService } from '../../service/AuthService';
import { User } from '../../models/User';
import { Router } from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header.html',
  styleUrls: ['./header.css']
})
export class HeaderComponent implements OnInit {

  isLogin: boolean;
  user: User;
  isShowMenu: boolean;


  constructor(private authService: AuthService,
              private router: Router ) {

  }


  ngOnInit() {
    this.authService.getCurrent().subscribe(() => {
      this.onUpdateUser();
    });

    this.authService.userObservable.subscribe((user: User) => {
      if (user) {
        this.isLogin = true;
      } else {
        this.isLogin = false;
      }
      this.user = user;
    });
  }


  onLogout() {
    this.authService.logout().subscribe(() => {
      this.user = null;
      this.isLogin = false;
      this.isShowMenu = false;
      this.router.navigate(['/login']);
    });

  }

  onUpdateUser () {
    if (this.authService.user) {
      this.user = this.authService.user;
      this.isLogin = true;
    } else {
      this.user = null;
      this.isLogin = false;
    }
  }

}
