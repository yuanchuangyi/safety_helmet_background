
import {Component, Injectable, OnInit} from '@angular/core';

import { CompanyService } from '../../service/CompanyService';
import { WorkplaceService } from '../../service/WorkplaceService';
import { JobuserService } from '../../service/jobuserService';

import { WorkProject } from '../../models/WorkProject';

@Component({
  templateUrl: 'videoMonitor.html',
  styleUrls: ['videoMonitor.css'],
})
export class VideoMonitorComponent implements OnInit {
  companyName: String;
  workprojects: WorkProject[];
  showProject: boolean = true;
  constructor( private companyService: CompanyService,
               private workplaceService: WorkplaceService,
               private jobuserService: JobuserService) {
  }
  ngOnInit() {
    this.companyService.getCompany().subscribe((res) => {
      this.companyName = res.res[0].companyName;
    })
  }

  getWorkers(workproject) {
    const jobId = null;
    const status = 1;
    const projectId = workproject.id;
    this.jobuserService.getjobuser(0, 5, {jobId, projectId, status}).subscribe((res) =>{
      workproject.workers = res.res;
      workproject.showWorkers = true;
    })
  }
  getProjects() {
    this.workplaceService.getmylist().subscribe((res) => {
      this.workprojects = res.res;
    })
    this.showProject = false;
  }

  hideProjects() {
    this.showProject = true;
  }
  hideWorker(workproject) {
    workproject.showWorkers = false;
  }
}
