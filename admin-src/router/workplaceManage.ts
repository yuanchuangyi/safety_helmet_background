import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WorkplaceManageComponent } from '../components/workplaceManage/workplaceManage';
import { AuthGuardService } from '../service/AuthGuardService';
import { WorkplaceResolveService } from '../service/WorkplaceResolveService';
import { WorkplaceDetailComponent } from '../components/workplaceDetail/workplaceDetail';

const routes: Routes = [
  {
    path: 'workplaceManage',
    canActivate: [AuthGuardService],
    children: [
      {
        path: '',
        redirectTo: 'unreview',
        pathMatch: 'full'
      },
      {
        path: 'unreview',
        component: WorkplaceManageComponent,
        resolve: {
          result: WorkplaceResolveService
        }
      },
      {
        path: 'published',
        component: WorkplaceManageComponent,
        resolve: {
          result: WorkplaceResolveService
        }
      },
      {
        path: 'unpassed',
        component: WorkplaceManageComponent,
        resolve: {
          result: WorkplaceResolveService
        }
      },
      {
        path: 'undershelf',
        component: WorkplaceManageComponent,
        resolve: {
          result: WorkplaceResolveService
        }
      },
      {
        path: 'workplaceDetail/:id',
        component: WorkplaceDetailComponent,
        resolve: {
          result: WorkplaceResolveService
        }
      }
    ]
  }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class WorkplaceManageRoutingModule {}
