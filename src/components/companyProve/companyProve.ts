import {Component, AfterViewChecked, OnInit,  ViewChild, Input} from '@angular/core';
import { NgForm, FormControl } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA , MatDatepickerInputEvent } from '@angular/material';
import { Company } from '../../models/Company';
import { CompanyService } from '../../service/CompanyService';
import { Moment } from 'moment';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmDialogComponent } from '../confirmDialog/confirmDialog';

import { CheckStatus,  CheckStatusMap } from '../../models/Company';
@Component({
  selector: 'app-company-prove',
  templateUrl: 'companyProve.html',
  styleUrls: ['companyProve.css'],
})
export class CompanyProveComponent implements OnInit,  AfterViewChecked {
  @Input() company: Company = {};
  @Input() hasCompany: boolean;
  messages = {
    companyName: '',
    companyCorporation: '',
    companyNum: '',
    description: '',
    employeeNum: '',
    address: '',
    phone: '',
  };
  canEdit: boolean;
  CheckStatus = CheckStatus;
  CheckStatusMap = CheckStatusMap;
  constructor(
    private companyService: CompanyService,
    private router: Router,
    private dialog: MatDialog
  ) {
  }

  ngAfterViewChecked() {
    if (this.companyForm) {
      this.companyForm.valueChanges.subscribe(data => this.checkValidate());
    }
  }

  ngOnInit() {
      this.canEdit = this.hasCompany;
      this.company.createDateShow = new FormControl(moment(this.company.createDate));
      this.company.createDate = moment(this.company.createDate).toDate();
  }

  onSelectDate( event: MatDatepickerInputEvent<Moment>) {
    this.company.createDate = event.value.toDate();
    this.company.createDateShow = new FormControl(event.value);
  }

  onDeleteCompany(id) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent,{
      width: '400px',
      height: '200px',
      data: {message: '确认删除？'}
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.companyService.delCompany(id).subscribe((res) => {
          if (res.success) {
           this.router.navigate(['home'])
          }
        })
      }
    })
  }

  onEditCompany() {
    this.canEdit = false;
  }

  cancel() {
    this.canEdit = true;
    this.router.navigate(['/home']);
  }
  onSubmit() {
    this.checkValidate(true);
    if (this.companyForm.invalid) {
      // 触发mat的error
      this.companyForm.onSubmit(null);
      return false;
    }
    if (this.company.id !== undefined) {
      delete this.company.updateDate;
      this.companyService.updateCompany(this.company).subscribe((res) => {
        if (res.success) {
          this.router.navigate(['home']);
          this.canEdit = this.hasCompany;
        }
      })
    } else {
      this.companyService.insertCompany(this.company).subscribe((res) => {
        if (res.success) {
          this.router.navigate(['/home']);
        }
      })
    }

  }
  checkValidate(isSubmit?: boolean) {
    const form = this.companyForm;

    for (const field in CompanyProveComponent.ErrorMessages) {

      const control = form.controls[field];

      if (control && control.valid) {
        this.messages[field] = '';
      }
      if (control && (control.dirty || isSubmit) && !control.valid) {
        const messages = CompanyProveComponent.ErrorMessages[field];
        if (control.errors) {
          this.messages[field] = messages[Object.keys(control.errors)[0]];
        }
      }
    }
  }
  @ViewChild('companyForm')
  companyForm: NgForm;
  static ErrorMessages = {
    companyName: {
      required: '公司名称不能为空'
    },
    companyCorporation: {
      required: '法人不能为空'
    },
    companyNum: {
      required: '企业代码不能为空'
    },
    description: {
      required: '企业描述不能为空'
    },
    employeeNum: {
      required: '员工人数不能为空',
      pattern: '格式不正确，只能输入数字'
    },
    address: {
      required: '企业地址不能为空'
    },
    phone: {
      required: '企业电话不能为空',
      pattern: '手机号格式不正确'
    }
  };
}
