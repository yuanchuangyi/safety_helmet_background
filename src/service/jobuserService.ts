
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import {map, switchMapTo} from 'rxjs/operators';
import { environment } from '../environments/environment';
import { User } from '../models/User';

@Injectable()
export class JobuserService {
  private baseUrl = environment.apiUrl + 'api/workprojectjobuser';
  constructor(
    private http: HttpClient
  ) {
  }
  getjobuser( current: number = 0, pageSize: number = environment.pageSize, options: {jobId?: number,  projectId?: number, status?: number  } = { status: 1}): Observable<{res: User[], success: boolean, total: number}> {
    const paramsObj: any = {
      pageSize: String(pageSize),
      page: String(current + 1),
      status: String(options.status),
    };
    if (options.projectId) paramsObj.projectId = options.projectId;
    if (options.jobId) paramsObj.jobId = options.jobId;
    const params = new HttpParams({ fromObject: paramsObj });
    return this.http.get<{res: User[], success: boolean, total: number}>(`${this.baseUrl}/list`,{params: params});
  }

  updateStatus(ids?: string, status?: number): Observable<{res: number, success: boolean}> {
      let params: HttpParams = new HttpParams();
      params = params.append('ids', String(ids));
      params = params.append('status', String(status));
      return this.http.get<{res: number, success: boolean}>(`${this.baseUrl}/updatestatus`, {params: params});
  }

}
