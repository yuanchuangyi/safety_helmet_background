import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Video } from '../../src/models/Video';
import { VideoService } from './VideoService';


import { environment } from '../../src/environments/environment';

import { CheckStatus } from '../../src/models/Company';

@Injectable()
export class VideoResolveService implements Resolve<{ res: Video[], total: number}> {

  constructor(private videoService: VideoService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{ res: Video[], total: number}>  {

    let status: CheckStatus;
    if (state.url.indexOf('published') > 0) {
      status = CheckStatus.PASS;
    } else if (state.url.indexOf('unpassed') > 0) {
      status = CheckStatus.UNPASS;
    } else if (state.url.indexOf('undershelf') > 0) {
      status = CheckStatus.UNDERSHELF;
    } else  {
      status = CheckStatus.UNREVIEW;
    }
    return this.videoService.getAllVideos(1, environment.adminPageSize, { status }).pipe(map((res) => {
      if (res.success) {
        return  res;
      } else {
        console.error(res.errorMsg);
      }
    }));
  }
}
