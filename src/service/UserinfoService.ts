
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import {map, switchMapTo} from 'rxjs/operators';
import { environment } from '../environments/environment';
import { UserRealinfo } from '../models/UserRealinfo';

@Injectable()
export class UserinfoService {
  private baseUrl = environment.apiUrl + 'api/sysuserrealinfo';
  private imgUrl = environment.apiUrl + 'api/fileupload/img';
  constructor(
    private http: HttpClient
  ) {
  }
  getoneInfo(userid): Observable<{res: UserRealinfo, success: boolean}> {
    return this.http.get<{res: UserRealinfo, success: boolean}>(`${this.baseUrl}/getone/${userid}`);
  }

  insertInfo(userRealinfo): Observable<{ success: boolean, errorMsg: string}> {
    return this.http.post<{ success: boolean, errorMsg: string}>(`${this.baseUrl}/insert`, userRealinfo);
  }
}
