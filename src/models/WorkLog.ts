import { User } from './User';
import { WorkProject } from './WorkProject';
import { Workjob } from './Workjob';

export interface WorkLog{
  content?: string;
  createDate?: Date;
  id?: number;
  imgUrl?: string;
  jobId?: number;
  projectId?: number;
  sysUser?: User;
  title?: string;
  userId?: number;
  workProject: WorkProject;
  workProjectJob: Workjob
}
