import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CompanyService } from '../../service/CompanyService';
import { Company } from '../../../src/models/Company';
import { CheckStatus } from '../../../src/models/Company';
import { ConfirmDialogComponent } from '../../../src/components/confirmDialog/confirmDialog';
import { MatDialog, MatSnackBar } from '@angular/material';
import { User } from '../../../src/models/User';

@Component({
  templateUrl: 'companyDetail.html',
  styleUrls: ['companyDetail.css']
})
export class CompanyDetailComponent implements OnInit {
  companyId: number;
  company: Company;
  CheckStatus = CheckStatus;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private  companyService: CompanyService,
              private bar: MatSnackBar,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.companyId = +this.route.snapshot.paramMap.get('id');
    this.company = new Company();
    this.company.sysUser = new User();
    this.companyService.getoneComapny(this.companyId).subscribe((res) => {
      if (res.success) {
        this.company = res.res;
      }
    })
  }

  onReview(status: CheckStatus) {
    this.company.status = status;
    this.companyService.reviewCompanyStatus([{id: this.companyId, status, checkMsg: this.company.checkMsg}]).subscribe((res) => {
      if (res.success) {
       this.onReturn(status);
      } else {
          this.bar.open(res.errorMsg);
      }
    })
  }

  onDelete() {
      this.companyService.deleteCompany(this.companyId).subscribe((res) => {
        if (res.success) {
          alert('删除成功');
          // this.onReturn(this.company.status);
        } else {
          this.bar.open(res.errorMsg);
        }
      })
  }

  onReturn(status: CheckStatus) {
    let path: string;
    if (status === CheckStatus.UNPASS) {
      path = '../../unpassed';
    } else if (status === CheckStatus.UNDERSHELF) {
      path = '../../undershelf';
    } else if (status === CheckStatus.UNREVIEW) {
      path = '../../unreview';
    } else {
      path = '../../published';
    }
    this.router.navigate([path], { relativeTo: this.route });
  }


}
