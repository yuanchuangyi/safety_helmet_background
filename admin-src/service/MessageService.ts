
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import {map, switchMapTo} from 'rxjs/operators';
import { environment } from '../environments/environment';
import { WorkMsg } from '../../src/models/WorkMsg';

import { CheckStatus } from '../../src/models/Company';

@Injectable()
export class MessageService {
  private baseUrl = environment.apiUrl + 'api/sysmsg';

  constructor(
    private http: HttpClient
  ) {
  }

  getAllMessages(current: number = 1, pageSize: number = environment.pageSize)
    : Observable<{ success: boolean, errorMsg: string, res: WorkMsg[], total: number }> {
    const paramsObj: any = {
      page: current,
      total: pageSize
    };
    const params = new HttpParams({ fromObject: paramsObj });
    return this.http.get<{ success: boolean, errorMsg: string, res: WorkMsg[], total: number }>(`${this.baseUrl}/getList`, { params });
  }

  insertMessage(message: WorkMsg): Observable<{success: boolean; res: WorkMsg, errorMsg: string}> {
    return this.http.post<{ success: boolean, res: WorkMsg, errorMsg: string}>(`${this.baseUrl}/insert`, message);
  }

}
