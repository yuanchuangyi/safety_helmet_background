import { Component, OnInit } from '@angular/core';

import { UserRealinfo } from '../../../src/models/UserRealinfo';
import { environment } from '../../../src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';

import { ConfirmDialogComponent } from '../../../src/components/confirmDialog/confirmDialog';
import { MatDialog, MatSnackBar } from '@angular/material';
import { RealinfoService } from '../../service/RealinfoService';
import {CheckStatus} from '../../../src/models/Company';
import {tap} from 'rxjs/operators';
import {adjustList, trackById} from '../../../src/others/utils';

@Component({
  templateUrl: './realnameManage.html',
  styleUrls: ['./realnameManage.css']
})
export class RealnameManageComponent implements OnInit {
  status: CheckStatus;
  realinfos: UserRealinfo[];
  realinfo: UserRealinfo;
  total: number;
  currentPage = 1;
  pageSize = environment.adminPageSize;
  environment = environment;
  trackById = trackById;
  constructor(
    private route: ActivatedRoute,
    private realinfoService: RealinfoService,
    private dialog: MatDialog,
    private bar: MatSnackBar,
    private router: Router,
  ) {}

  ngOnInit() {
    this.route.data.subscribe((data: { result: { total: number, res: UserRealinfo[] } }) => {
      this.realinfos = data.result.res;
      this.total = data.result.total;
    });
    this.realinfo = {};
    const url = this.router.routerState.snapshot.url;
    if (url.indexOf('published') > 0) {
      this.status = CheckStatus.PASS;
    } else if (url.indexOf('unpassed') > 0) {
      this.status = CheckStatus.UNPASS;
    } else if (url.indexOf('undershelf') > 0) {
      this.status = CheckStatus.UNDERSHELF;
    } else  {
      this.status = CheckStatus.UNREVIEW;
    }
  }


  onPageChanged(page: number) {
    this.update(page, this.status).subscribe();
  }

  update(page: number, status?: CheckStatus) {
    const options: any = { status };
    return this.realinfoService.getAllRealinfos(page, this.pageSize, options).pipe(tap((res) => {
      if (res.success) {
        this.currentPage = page;
        this.total = res.total;
        // this.companys = res.res;
        this.realinfos = adjustList(res.res, 4);
      } else {
        this.bar.open(res.errorMsg);
      }
    }));
  }
}
