import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from '../components/home/home';
import { LoginComponent} from '../components/login/login';
import { CompanyDetailComponent } from '../components/companyDetail/companyDetail';
import { AuthGuardService } from '../service/AuthGuardService';
import { CompanyResolveService } from '../service/CompanyResolveService';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    canActivate: [AuthGuardService],
    children: [
      {
        path: '',
        redirectTo: 'unreview',
        pathMatch: 'full'
      },
      {
        path: 'unreview',
        component: HomeComponent,
        resolve: {
          result: CompanyResolveService
        }
      },
      {
        path: 'published',
        component: HomeComponent,
        resolve: {
          result: CompanyResolveService
        }
      },
      {
        path: 'unpassed',
        component: HomeComponent,
        resolve: {
          result: CompanyResolveService
        }
      },
      {
        path: 'undershelf',
        component: HomeComponent,
        resolve: {
          result: CompanyResolveService
        }
      },
      {
        path: 'companyDetail/:id',
        component: CompanyDetailComponent,
        resolve: {
          result: CompanyResolveService
        }
      },
    ]
  },

  {
    path: 'login',
    component: LoginComponent
  }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
