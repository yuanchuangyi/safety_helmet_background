
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import {map, switchMapTo} from 'rxjs/operators';
import { environment } from '../environments/environment';
import { User } from '../../src/models/User';

import { CheckStatus } from '../../src/models/Company';

@Injectable()
export class WorkerTopService {
  private baseUrl = environment.apiUrl + 'api/sysuser';

  constructor(
    private http: HttpClient
  ) {
  }

  getAllWorkertops(current: number = 1, pageSize: number = environment.pageSize)
    : Observable<{ success: boolean, errorMsg: string, res: User[], total: number }> {
    const paramsObj: any = {
      page: current,
      pageSize: pageSize,
    };
    const params = new HttpParams({ fromObject: paramsObj });
    return this.http.get<{ success: boolean, errorMsg: string, res: User[], total: number }>(`${this.baseUrl}/getforemanlist`, { params });
  }

  updateStatus(workerTop: User): Observable<{res: User, success: boolean}> {
    return this.http.patch<{ res: User, success: boolean}>(`${this.baseUrl}/updatestatus`, workerTop);
  }

}
