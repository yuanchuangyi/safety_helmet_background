import { BrowserModule, Title } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { HomeModule } from './home.module';
import { AuthModule } from './auth.module';
import { ShareModule } from './share.module';
import { WorkerModule } from './worker.module';
import { WorkplaceModule} from './workplace.module';
import { CompanyModule } from './company.module';
import { WorkjobModule } from './workjob.module';
import { WorklogModule } from './worklog.module';
import { VideoMonitorModule} from './videoMonitor.module';
import { WorkChatModule } from './workChat.module';
import { AppRoutingModule } from '../router/app';


import { DialogService } from '../service/DialogService';

import { AppComponent } from '../components/app/app';
import { HeaderComponent } from '../components/header/header';
import { PageNotFoundComponent } from '../components/pageNotFound/pageNotFound';
import { ConfirmDialogComponent } from '../components/confirmDialog/confirmDialog';


@NgModule({
  imports: [
    BrowserModule, AppRoutingModule, ShareModule, HomeModule, AuthModule, HttpClientModule, WorkerModule, WorkplaceModule, WorkjobModule, CompanyModule, WorklogModule, VideoMonitorModule, WorkChatModule
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    PageNotFoundComponent,
    ConfirmDialogComponent
  ],
  entryComponents: [ ConfirmDialogComponent ],
  providers: [
    DialogService,
    Title,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
