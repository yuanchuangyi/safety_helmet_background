
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import {map, switchMapTo} from 'rxjs/operators';
import { environment } from '../environments/environment';
import { WorkEvaluate } from '../models/WorkEvaluate';
import {WorkMsg} from '../models/WorkMsg';

@Injectable()
export class WorkEvaluateService {
  private baseUrl = environment.apiUrl + 'api/workevaluation';
  constructor(
    private http: HttpClient
  ) {
  }

  deleteEvaluate(id): Observable<{success: boolean}> {
    return this.http.delete<{success: boolean}>(`${this.baseUrl}/del/${id}`);
  }

  insertEvaluate(evaluate: WorkEvaluate): Observable<{res: WorkEvaluate, success: boolean}> {
    return this.http.post<{res: WorkEvaluate, success: boolean}>(`${this.baseUrl}/insert`, evaluate);
  }

  getEvaluate( current: number = 0, pageSize: number = environment.pageSize, type: number, options: {companyId?: number, projectId?: number, evaluatioinUserId?: number, userId?: number}) {
    const paramsObj: any = {
      pageSize: String(pageSize),
      page: String(current),
      type: String(type)
    };
    if (options.companyId) paramsObj.companyId = options.companyId;
    if (options.projectId) paramsObj.projectId = options.projectId;
    if (options.evaluatioinUserId) paramsObj.evaluatioinUserId = options.evaluatioinUserId;
    if (options.userId) paramsObj.userId = options.userId;
    const params = new HttpParams({fromObject: paramsObj});
    return this.http.get<{res: WorkEvaluate[], success: boolean, total: number}> (`${this.baseUrl}/list`,{params: params});
  }

  updateEvaluate(evaluate: WorkEvaluate): Observable<{res: WorkEvaluate, success: boolean}> {
    return this.http.patch<{res: WorkEvaluate, success: boolean}> (`${this.baseUrl}/update`, evaluate);
  }
}
