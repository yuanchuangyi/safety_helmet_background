import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  templateUrl: './confirmDialog.html',
  styleUrls: ['./confirmDialog.css']
})
export class ConfirmDialogComponent implements OnInit {

  message: string;

  constructor(
    private dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { message: string }
  ) {
    this.message = data.message;
  }


  ngOnInit() {

  }

  onConfirm() {
    this.dialogRef.close(true);
  }

}
