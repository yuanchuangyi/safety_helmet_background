import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuardService } from '../service/AuthGuardService';
import { WorkplaceResolveService } from '../service/WorkplaceResolveService';
import { WorkplaceComponent } from '../components/workplace/workplace';

const routes: Routes = [
  {
    path: 'workplace',
    canActivate: [AuthGuardService],
    component: WorkplaceComponent,
    resolve: {
      results: WorkplaceResolveService
    }
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ],


})
export class WorkplaceRoutingModule {}
