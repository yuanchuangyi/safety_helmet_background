import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from '../../service/AuthService';
import { User } from '../../../src/models/User';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-header',
  templateUrl: './header.html',
  styleUrls: ['./header.css']
})
export class HeaderComponent implements OnInit {
  @Input()
  haveInput: boolean;
  user: User;
  constructor (
    private authService: AuthService,
    private router: Router
  ) {

  }
  ngOnInit() {
    this.user = this.authService.user;
  }

  onLogout() {
    this.authService.logout().subscribe(() => {
      this.router.navigateByUrl('/login');
    });
  }

}
