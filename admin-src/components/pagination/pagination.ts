import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.html',
  styleUrls: ['./pagination.css']
})
export class PaginationComponent implements OnInit {

  @Input()
  maxSize: number;
  @Input()
  isShowInput: boolean;
  @Output()
  pageChange = new EventEmitter<number>();
  @Input()
  isMiniMode: boolean;
  @Input()
  total: number;


  ngOnInit() {
  }
}
