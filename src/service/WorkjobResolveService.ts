//
// import { Injectable } from '@angular/core';
// import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
// import { Observable } from 'rxjs';
// import { map } from 'rxjs/operators';
//
// import { WorkjobService } from './WorkjobService';
// import { Workjob } from '../models/Workjob';
//
// @Injectable()
// export class WorkjobResolveService implements Resolve<{total: number, workjobs: Workjob[]}> {
//
//   constructor(private workjobService: WorkjobService) {}
//
//   resolve(route: ActivatedRouteSnapshot): Observable<{total: number, workjobs: Workjob[]}>  {
//
//     return this.workjobService.getalljob().pipe(map(res => {
//       if (res.success) {
//         return  {total: res.total, workjobs: res.res};
//       } else {
//         console.error(res.res);
//         return {total: 0, workjobs: []};
//       }
//     }));
//   }
// }
