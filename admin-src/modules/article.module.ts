import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ShareModule } from './share.module';

import { ArticleService } from '../service/ArticleService';
import { ArticleResolveService } from '../service/ArticleResolveService';
import { ArticleRoutingModule } from '../router/article';
import { ArticleListComponent } from '../components/articleList/articleList';
import { ArticleDetailComponent } from '../components/articleDetail/articleDetail';

@NgModule({
  imports: [BrowserModule, ShareModule, ArticleRoutingModule],
  declarations: [
    ArticleListComponent,
    ArticleDetailComponent
  ],
  providers: [
    ArticleService,
    ArticleResolveService
  ]
})
export class ArticleModule { }
