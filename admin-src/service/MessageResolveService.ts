import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { WorkMsg } from '../../src/models/WorkMsg';
import { MessageService } from './MessageService';

import { environment } from '../../src/environments/environment';

import { CheckStatus } from '../../src/models/Company';

@Injectable()
export class MessageResolveService implements Resolve<{ res: WorkMsg[], total: number}> {

  constructor(private messageService: MessageService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{ res: WorkMsg[], total: number}>  {
    return this.messageService.getAllMessages(1, environment.adminPageSize).pipe(map((res) => {
      if (res.success) {
        return  res;
      } else {
        console.error(res.errorMsg);
      }
    }));
  }
}
