export class UserRealinfo {
  address?: string;
  cardUrl1?: string;
  cardUrl2?: string;
  certificateAuthority?: string;
  id?: string;
  personNum?: string;
  realName?: string;
  typeOfId?: PaperType;
  userid?: number;
  validityDate?: Date;
  validityDateShow?: any;
  createDate?: Date;
  createDateShow?: any;
  updateDate?: Date;
  status?: number;
  checkMsg?: string;
}
export enum PaperType {
  Choose,
  Identity,
  Officer,
  Student,
  Other
}

export const  paperType = [PaperType.Choose, PaperType.Identity, PaperType.Officer, PaperType.Student, PaperType.Other];

export const TypeNameMap = {
  [PaperType.Choose]: '请选择',
  [PaperType.Identity]: '身份证',
  [PaperType.Officer]: '军官证',
  [PaperType.Student]: '学生证',
  [PaperType.Other]: '其他'
}
