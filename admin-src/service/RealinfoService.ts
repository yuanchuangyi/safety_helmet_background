
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import {map, switchMapTo} from 'rxjs/operators';
import { environment } from '../environments/environment';
import { UserRealinfo } from '../../src/models/UserRealinfo';
import { CheckStatus } from '../../src/models/Company';
import {Article} from '../../src/models/Article';

@Injectable()
export class RealinfoService {
  private baseUrl = environment.apiUrl + 'api/sysuserrealinfo';

  constructor(
    private http: HttpClient
  ) {
  }

  getAllRealinfos(current: number = 1, pageSize: number = environment.pageSize, options: { status?: CheckStatus } = { status: CheckStatus.UNREVIEW })
    : Observable<{ success: boolean, errorMsg: string, res: UserRealinfo[], total: number }> {
    const paramsObj: any = {
      status: String(options.status),
      page: current,
      pageSize: pageSize
    };
    const params = new HttpParams({ fromObject: paramsObj });
    return this.http.get<{ success: boolean, errorMsg: string, res: UserRealinfo[], total: number }>(`${this.baseUrl}/getList`, { params: params });
  }

  getOneRealinfo(id): Observable<{res: UserRealinfo, success: boolean, errorMsg: string}> {
    return this.http.get<{res: UserRealinfo, success: boolean, errorMsg: string}>(`${this.baseUrl}/getone/${id}`);
  }

  reviewRealinfo(userRealinfo: Array<UserRealinfo>): Observable<{res: UserRealinfo, success: boolean, errorMsg: string}> {
    return this.http.patch<{res: UserRealinfo, success: boolean, errorMsg: string}>(`${this.baseUrl}/review`, userRealinfo);
  }

}
