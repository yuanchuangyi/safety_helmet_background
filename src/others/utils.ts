
export const adjustList = function (list: any[], lineSize: number): any[] {
  const rest = (list.length) % lineSize;
  if (rest !== 0) {
    for (let i = 0; i < lineSize - rest ; i++) {
      list.push({});
    }
  }
  return list;
};

export const trackById = function(index: number, item: any): any {
  return item.id;
};
