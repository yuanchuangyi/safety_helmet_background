export const environment = {
  production: true,
  apiUrl: '',
  pageSize: 20,
};

export const myFormats = {
  parse: {
    dateInput: 'LL'
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'YYYY-MM',
    dateA11yLabel: 'DD',
    monthYearA11yLabel: 'YYYY-MM',
  }
};
