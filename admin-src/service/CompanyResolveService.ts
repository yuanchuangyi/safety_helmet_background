import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Company } from '../../src/models/Company';
import { CompanyService } from './CompanyService';

import { environment } from '../../src/environments/environment';

import { CheckStatus } from '../../src/models/Company';

@Injectable()
export class CompanyResolveService implements Resolve<{ res: Company[], total: number}> {

  constructor(private companyService: CompanyService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{ res: Company[], total: number}>  {

    let status: CheckStatus;
    if (state.url.indexOf('published') > 0) {
      status = CheckStatus.PASS;
    } else if (state.url.indexOf('unpassed') > 0) {
      status = CheckStatus.UNPASS;
    } else if (state.url.indexOf('undershelf') > 0) {
      status = CheckStatus.UNDERSHELF;
    } else  {
      status = CheckStatus.UNREVIEW;
    }
    return this.companyService.getAllCompanys(1, environment.adminPageSize, { status }).pipe(map((res) => {
      if (res.success) {
        return  res;
      } else {
        console.error(res.errorMsg);
      }
    }));
  }
}
