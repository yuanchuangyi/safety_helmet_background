/**
 * Created by jixianyan on 2017/10/19.
 */

export enum DisplayType {
    THREED,
    TWOD,
    AUDIO,
    VIDEO,
    UPLOAD
}

export const MaterialPerPage = 8;

export enum MaterialType {
    THREED,
    TWOD,
    AUDIO,
    VIDEO,
    ALL
}

export function NameMaterialTypeMap(type: string): MaterialType {
    switch (type) {
        case 'three':
            return MaterialType.THREED;
        case 'two':
            return MaterialType.TWOD;

        case 'audio':
            return MaterialType.AUDIO;

        case 'video':
            return MaterialType.VIDEO;
        default:
            return MaterialType.ALL;

    }
}

export function MaterialTypeNameMap(type: MaterialType): string {
    switch (type) {
        case MaterialType.THREED:
            return 'three';
        case MaterialType.TWOD:
            return 'two';
        case MaterialType.AUDIO:
            return 'audio';
        case MaterialType.VIDEO:
            return 'video';
        default:
            return 'all';

    }
}

export enum ThreeDFormat {
    MAX,
    OBJ,
    MAorMB,
    THREEDS,
    C4D,
    ZTL,
    THREEDM,
    STL,
    SKP,
    FBX,
    OTHER
}

export  const ThreeDFormatMap = ['max', 'obj', 'ma/mb', '3ds', 'c4d', 'ztl', '3dm', 'stl', 'skp', 'fbx', '其他'];


export enum TwoDFormat {
    TEXT,
    DOC,
    EXCEL,
    PPT,
    GIF,
    ICON,
    JPEG,
    PNG,
    TIFF,
    PSD,
    OTHER
}

export  const TwoDFormatMap = ['text', 'doc', 'excel', 'ppt', 'gif', 'icon', 'jpeg', 'png', 'tiff', 'psd', '其他'];


export enum AudioFormat {
    AAC,
    APE,
    MID,
    CDA,
    MP3,
    MPEG,
    FLAC,
    WAV,
    WMA,
    OTHER
}

export const  AudioFormatMap = ['aac', 'ape', 'mid', 'cda', 'mp3', 'mpeg', 'flac', 'wav', 'wma', '其他'];

export enum VideoFormat {
    AVI,
    ASF,
    MP4,
    MPEG,
    WMV,
    FLV,
    MOV,
    THREEGP,
    MKV,
    RMVB,
    WEBM,
    F4V,
    BLURAY,
    OTHER
}

export const VideoFormatMap = ['avi', 'asf', 'mp4', 'mpeg', 'wmv', 'flv', 'mov', '3gp', 'mkv', 'rmvb', 'webm', 'f4v', 'blu-ray', '其他'];
