import { Component, OnInit, Inject, AfterViewChecked, ViewChild } from '@angular/core';
import {FormControl, NgForm} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDatepickerInputEvent } from '@angular/material';
import { Moment } from 'moment';
import * as moment from 'moment';

import { WorkEvaluate } from '../../models/WorkEvaluate';
import { WorkplaceService } from '../../service/WorkplaceService';
import { WorkEvaluateService } from '../../service/WorkEvaluateService';
import { AuthService } from '../../service/AuthService';

@Component({
  templateUrl: './evaluateAdd.html',
  styleUrls: ['./evaluateAdd.css']
})
export class  EvaluateAddComponent implements OnInit, AfterViewChecked {

  messages = {
    comment: '',
    num: ''
  };
  stars: number;
  evaluate: WorkEvaluate;
  companyId: number;
  projectId: number;
  userId: number;
  constructor(
    private dialogRef: MatDialogRef<EvaluateAddComponent>,
    private workplaceService: WorkplaceService,
    private workEvaluateService: WorkEvaluateService,
    private authService: AuthService,
    // private companyService: CompanyService,
    @Inject(MAT_DIALOG_DATA) public data: {companyId: number, projectId: number, userId: number}
  ) {
      this.companyId = data.companyId;
      this.projectId = data.projectId;
      this.userId = data.userId;
  }

  @ViewChild('evaluateForm')
  evaluateForm: NgForm;

  ngAfterViewChecked() {
    if (this.evaluateForm) {
      this.evaluateForm.valueChanges.subscribe(data => this.checkValidate());
    }
  }
  ngOnInit() {
      this.evaluate = new WorkEvaluate();
      this.authService.getCurrent().subscribe((res) => {
        this.evaluate.evaluationUserId = res.res.id;
      })
  }

  clickStars(i: number) {
    this.stars = i;
    if (this.stars === 1) {
      this.evaluate.num1 = 1;
      this.evaluate.num2 = 0;
      this.evaluate.num3 = 0;
      this.evaluate.num4 = 0;
      this.evaluate.num5 = 0;
    } else if (this.stars === 2) {
      this.evaluate.num1 = 1;
      this.evaluate.num2 = 1;
      this.evaluate.num3 = 0;
      this.evaluate.num4 = 0;
      this.evaluate.num5 = 0;
    } else if (this.stars === 3) {
      this.evaluate.num1 = 1;
      this.evaluate.num2 = 1;
      this.evaluate.num3 = 1;
      this.evaluate.num4 = 0;
      this.evaluate.num5 = 0;
    } else if (this.stars === 4) {
      this.evaluate.num1 = 1;
      this.evaluate.num2 = 1;
      this.evaluate.num3 = 1;
      this.evaluate.num4 = 1;
      this.evaluate.num5 = 0;
    } else if (this.stars === 5) {
      this.evaluate.num1 = 1;
      this.evaluate.num2 = 1;
      this.evaluate.num3 = 1;
      this.evaluate.num4 = 1;
      this.evaluate.num5 = 1;
    }
  }

  onSubmit() {
    this.evaluate.companyId = this.companyId;
    this.evaluate.projectId = this.projectId;
    this.evaluate.userId = this.userId;
    this.evaluate.type = 2;
    this.checkValidate(true);
    if (this.evaluateForm.invalid) {
      // 触发mat的error
      this.evaluateForm.onSubmit(null);
      return false;
    }
    if (this.evaluate.id !== undefined) {
      this.workEvaluateService.updateEvaluate(this.evaluate).subscribe((res) => {
        this.dialogRef.close(res.res);
      })
    } else {
      this.workEvaluateService.insertEvaluate(this.evaluate).subscribe((res) => {
        this.dialogRef.close(res.res);
      })
    }
  }

  checkValidate(isSubmit?: boolean) {
    const form = this.evaluateForm;

    for (const field in EvaluateAddComponent.ErrorMessages) {

      const control = form.controls[field];

      if (control && control.valid) {
        this.messages[field] = '';
      }
      if (control && (control.dirty || isSubmit) && !control.valid) {
        const messages = EvaluateAddComponent.ErrorMessages[field];
        if (control.errors) {
          this.messages[field] = messages[Object.keys(control.errors)[0]];
        }
      }
    }
  }

  static ErrorMessages = {
    comment: {
      required: '内容不能为空'
    },
    num: {
      required: '评分不能为空',
      pattern: '格式不正确，只能输入数字'
    }
  };
}
