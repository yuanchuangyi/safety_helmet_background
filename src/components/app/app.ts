import { Component, HostBinding, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Title } from '@angular/platform-browser';


@Component({
  selector: 'body',
  templateUrl: 'app.html',
  styleUrls: ['app.css']
})
export class AppComponent implements OnInit {

  isLogin: Boolean = false;


  @HostBinding('class')
  color: string = '';


  constructor(private router: Router, private titleService: Title) {
  }

  ngOnInit() {

    this.titleService.setTitle( '安全帽工长管理系统' );

  }




}
