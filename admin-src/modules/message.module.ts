import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ShareModule } from './share.module';

import { MessageService } from '../service/MessageService';
import { FileService } from '../service/FileService';
import { MessageResolveService } from '../service/MessageResolveService';
import { EditorDeactivateGuardService } from '../service/EditorDeactivateGuardService';
import { MessageManageRoutingModule } from '../router/messageManage';
import { MessageManageComponent } from '../components/messageManage/messageManage';
import { AddMessageComponent } from '../components/addMessage/addMessage';
import { EditorComponent } from '../components/editor/editor';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
@NgModule({
  imports: [  BrowserModule,
              ShareModule,
              MessageManageRoutingModule,
              FroalaEditorModule.forRoot(),
              FroalaViewModule.forRoot()
            ],
  declarations: [
    MessageManageComponent,
    AddMessageComponent,
    EditorComponent
  ],
  providers: [
    FileService,
    MessageService,
    MessageResolveService,
    EditorDeactivateGuardService
  ]
})
export class MessageModule { }
