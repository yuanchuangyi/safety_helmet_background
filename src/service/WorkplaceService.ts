
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import {map, switchMapTo} from 'rxjs/operators';
import { environment } from '../environments/environment';
import { WorkProject } from '../models/WorkProject';

@Injectable()
export class WorkplaceService {
  private baseUrl = environment.apiUrl + 'api/workproject';
  constructor(
    private http: HttpClient
  ) {
  }

  deleteProject(id): Observable<{success: boolean}> {
    return this.http.delete<{success: boolean}>(`${this.baseUrl}/del/${id}`);
  }
  getoneProject(projectId): Observable<{res: WorkProject, success: boolean}> {
    return this.http.get<{res: WorkProject, success: boolean}>(`${this.baseUrl}/getone?id=${projectId}`);
  }
  insertProject(workProject: WorkProject): Observable<{res: WorkProject, success: boolean}> {
    return this.http.post<{res: WorkProject, success: boolean}>(`${this.baseUrl}/insert`, workProject);
  }
  getmylist(current: number = 0, pageSize: number = environment.pageSize, status: number = 0): Observable<{res: WorkProject[], success: boolean, total: number}> {
    let params: HttpParams = new HttpParams();
    params = params.append('page', String(current + 1));
    params = params.append('pageSize', String(pageSize));
    params = params.append('status', String(status));
    return this.http.get<{res: WorkProject[], success: boolean, total: number}>(`${this.baseUrl}/mylist`, { params : params});
  }
  updateProject(workProject: WorkProject): Observable<{res: WorkProject, success: boolean}> {
    delete workProject.updateDate;
    delete  workProject.createDate;
    return this.http.patch<{res: WorkProject, success: boolean}>(`${this.baseUrl}/update`, workProject);
  }
}
