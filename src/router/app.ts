import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageNotFoundComponent } from '../components/pageNotFound/pageNotFound';
import { LoginComponent } from '../components/login/login';
import { RegisterComponent } from '../components/register/register';
import { AuthGuardService } from '../service/AuthGuardService';
import { HomePageComponent} from '../components/homePage/homePage';
import { RealnameProveComponent } from '../components/realnameProve/realnameProve';
import { PersonInfoComponent } from '../components/personInfo/personInfo';
import { SecurityComponent } from '../components/security/security';
import { ForgetPasswordComponent } from '../components/forgetPassword/forgetPassword';
import { ChangePasswordComponent } from '../components/changePassword/changePassword';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomePageComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'realnameProve',
    component: RealnameProveComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'personInfo',
    component: PersonInfoComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'security',
    component: SecurityComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'findPassword',
    component: ForgetPasswordComponent
  },
  {
    path: 'changePassword',
    component: ChangePasswordComponent
  }
  // {
  //   path: '404',
  //   component: PageNotFoundComponent
  // },
  // {
  //   path: '**',
  //   component: PageNotFoundComponent
  // }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
