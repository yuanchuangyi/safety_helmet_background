
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import {map, switchMapTo} from 'rxjs/operators';
import { environment } from '../environments/environment';
import { Video } from '../../src/models/Video';
import { CheckStatus } from '../../src/models/Company';

@Injectable()
export class VideoService {
  private baseUrl = environment.apiUrl + 'api';


  constructor(
    private http: HttpClient
  ) {
  }

  getAllVideos(current: number = 1, pageSize: number = environment.pageSize, options: { status?: CheckStatus } = { status: CheckStatus.UNREVIEW })
    : Observable<{ success: boolean, errorMsg: string, res: Video[], total: number }> {
    const paramsObj: any = {
      status: String(options.status),
      page: current,
      pageSize: pageSize
    };
    const params = new HttpParams({ fromObject: paramsObj });
    return this.http.get<{ success: boolean, errorMsg: string, res: Video[], total: number }>(`${this.baseUrl}/cmsvideo/list`, { params });
  }

  getOneVideo(id): Observable<{res: Video, success: boolean, errorMsg: string}> {
    return this.http.get<{res: Video, success: boolean, errorMsg: string}>(`${this.baseUrl}/cmsvideo/getone/${id}`);
  }

  reviewVideo(video: Array<Video>): Observable<{res: Video, success: boolean, errorMsg: string}> {
    return this.http.patch<{res: Video, success: boolean, errorMsg: string}>(`${this.baseUrl}/cmsvideo/review`, video);
  }

  deleteVideo(id): Observable<{res: boolean, success: boolean, errorMsg: string}> {
    return this.http.delete<{res: boolean, success: boolean, errorMsg: string}>(`${this.baseUrl}/cmsvideo/del/${id}`);
  }
}
