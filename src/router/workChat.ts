import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { WorkChatComponent } from '../components/workChat/workChat';
import {AuthGuardService} from '../service/AuthGuardService';

const routes: Routes = [
  {
    path: 'workChat',
    component: WorkChatComponent,
    canActivate: [AuthGuardService],
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ],
})
export class WorkChatRoutingModule {}
