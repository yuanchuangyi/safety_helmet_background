
import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { MatDialog } from '@angular/material';

import { ConfirmDialogComponent } from '../../src/components/confirmDialog/confirmDialog';
import { map } from 'rxjs/operators';
import { AddMessageComponent } from '../components/addMessage/addMessage';


@Injectable()
export class EditorDeactivateGuardService implements CanDeactivate<AddMessageComponent> {

  constructor (
    private dialog: MatDialog,
  ) {}

  canDeactivate(component: AddMessageComponent, route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

    const editor = component.editor;
    // Allow synchronous navigation (`true`) if no crisis or the crisis is unchanged
    if (component.submitted || editor.untouched || !editor.content) {
      return of(true);
    }
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '300px',
      height: '165px',
      data: {message: '放弃保存?'}
    });
    // Otherwise ask the user with the dialog service and return its
    // promise which resolves to true or false when the user decides
    return dialogRef.afterClosed().pipe(map((res) => {
      return !!res;
    }));
  }
}
