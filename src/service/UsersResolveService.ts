
import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { UserService } from './UserService';
import { User } from '../models/User';

@Injectable()
export class UsersResolveService implements Resolve<{total: number, users: User[]}> {

  constructor(private userService: UserService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<{total: number, users: User[]}>  {

    return this.userService.getUsers().pipe(map(res => {
      if (res.success) {
        return  {total: res.total, users: res.res};
      } else {
        console.error(res.res);
        return {total: 0, users: []};
      }
    }));
  }
}

