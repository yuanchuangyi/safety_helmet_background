import { Component, ViewChild, AfterViewChecked } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { NgForm } from '@angular/forms';

import { AuthService } from '../../service/AuthService';
import { UserinfoService } from '../../service/UserinfoService';
import { User } from '../../models/User';


@Component({
  templateUrl: 'login.html',
  styleUrls: ['login.css'],
})
export class LoginComponent implements AfterViewChecked {
  static ErrorMessages = {
    username: {
      required: '账号不能为空',
      notExist: '账号不存在'
    },
    mima: {
      required: '密码不能为空',
      unMatched: '用户名密码不匹配',
      notWorkerTop: '只有工长才有权限登录'
    },
    validateCode: {
      required: '验证码不能为空',
      unMatched: '验证码错误',
    }
  };
  user: User;
  messages = {
    username: '',
    mima: '',
    validateCode: ''
  };
  isValidateCodeLogin: boolean = false;
  hasimg: boolean = true;
  randomNumber: number;
  @ViewChild('loginForm')
  loginForm: NgForm;
  baseurl : string = 'http://file.sxycy.cn:84/api/login/validateCode?';
  constructor(public authService: AuthService,
              private userinfoService: UserinfoService,
              public router: Router) {
    this.user = new User();
  }
  ngAfterViewChecked() {

    // Angular的“单向数据流”规则禁止在一个视图已经被组合好之后再更新视图.而此时已组合好
    if (this.loginForm) {
      this.loginForm.valueChanges.subscribe(data => this.checkValidate());
    }
  }
  login() {
    this.checkValidate(true);
    if (this.loginForm.invalid) {
      return false;
    }
    this.authService.login(this.user).subscribe((res) => {
      if (res.isValidateCodeLogin) {
        this.isValidateCodeLogin = true;
        // this.authService.getCode().subscribe((result) => {
        //   this.codeimg = result;
        // })
      }
        else if (res.success && this.authService.isWorkertop(res.res)) {
        this.authService.user = res.res;
        this.userinfoService.getoneInfo(res.res.id).subscribe((data) => {
          if (data.res) {
            const redirect: any[] = this.authService.redirectUrl ? [this.authService.redirectUrl]
              : ['/home'];
            const navigationExtras: NavigationExtras = {
              queryParamsHandling: 'preserve',
              preserveFragment: true
            };
            this.router.navigate(redirect, navigationExtras);
          } else{
            this.router.navigate(['/realnameProve'], {queryParams: {'userid': res.res.id}});
          }
        })
      }
      else if (res.success && !this.authService.isWorkertop(res.res)) {
        this.messages.mima = LoginComponent.ErrorMessages.mima.notWorkerTop;
      }
      else if (!res.success){
        this.messages.mima = LoginComponent.ErrorMessages.mima.unMatched;
      }
    });
  }

  changeimg() {
    this.randomNumber = Number(1000 * Math.random());
    this.hasimg = false;
    setTimeout( () => {
      this.hasimg = true;
    },100);
  }
  checkValidate(isSubmit?: boolean) {
    const form = this.loginForm;

    for (const field in LoginComponent.ErrorMessages) {

      const control = form.controls[field];

      if (control && control.valid) {
        this.messages[field] = '';
      }
      if (control && (control.dirty || isSubmit) && !control.valid) {
        const messages = LoginComponent.ErrorMessages[field];
        if (control.errors) {
          this.messages[field] = messages[Object.keys(control.errors)[0]];
        }
      }
    }
  }

}
