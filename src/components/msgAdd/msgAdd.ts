import { Component, OnInit, Inject, AfterViewChecked, ViewChild } from '@angular/core';
import {FormControl, NgForm} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDatepickerInputEvent } from '@angular/material';
import { Moment } from 'moment';
import * as moment from 'moment';
import { User } from '../../models/User';
import { UserRealinfo } from '../../models/UserRealinfo';
import { UserService } from '../../service/UserService';
import { WorkMsg } from '../../models/WorkMsg';
import { WorkmsgService } from '../../service/WorkmsgService';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import {environment} from '../../environments/environment';
@Component({
  templateUrl: './msgAdd.html',
  styleUrls: ['./msgAdd.css']
})
export class  MsgAddComponent implements OnInit, AfterViewChecked {
  uploader: FileUploader = new FileUploader({
    url: environment.apiUrl + 'api/fileupload/img',
    removeAfterUpload: true,
    method: 'POST'
  });
  messages = {
    content: '',
    type: '',
    status: ''
  };
  workMsg: WorkMsg;
  projectId: number;
  constructor(
    private dialogRef: MatDialogRef<MsgAddComponent>,
    private workmsgService: WorkmsgService,
    @Inject(MAT_DIALOG_DATA) public data: {projectId: number}
  ) {
    console.log(data.projectId);
    this.projectId = data.projectId;
  }

  @ViewChild('msgForm')
  msgForm: NgForm;

  ngAfterViewChecked() {
    if (this.msgForm) {
      this.msgForm.valueChanges.subscribe(data => this.checkValidate());
    }
  }
  ngOnInit() {
    this.workMsg = new WorkMsg();
  }

  ImgsOnChanged(event: any) {
    this.uploader.queue.forEach(queue => {
      queue.withCredentials = false;
      queue.onError = (response: string, status: number) => {
        console.log(response, status);
      };

      queue.onSuccess = (responses, status, headers) => {
        if (status === 200) {
          const response = JSON.parse(responses);
          this.workMsg.url =  response['full-url'];
        } else {
          console.log('err', Error);
        }
      };
      queue.upload();
    });
  }

  onSubmit() {
    this.checkValidate(true);
    if (this.msgForm.invalid) {
      // 触发mat的error
      this.msgForm.onSubmit(null);
      return false;
    }
    this.workMsg.projectId = this.projectId;
      this.workmsgService.insertMsg(this.workMsg).subscribe((res) => {
        if (res.success) {
          alert('新建成功');
          this.dialogRef.close(res.res);
        }
      });
  }


  checkValidate(isSubmit?: boolean) {
    const form = this.msgForm;

    for (const field in MsgAddComponent.ErrorMessages) {

      const control = form.controls[field];

      if (control && control.valid) {
        this.messages[field] = '';
      }
      if (control && (control.dirty || isSubmit) && !control.valid) {
        const messages = MsgAddComponent.ErrorMessages[field];
        if (control.errors) {
          this.messages[field] = messages[Object.keys(control.errors)[0]];
        }
      }

    }
  }

  static ErrorMessages = {
    content: {
      required: '内容不能为空'
    },
    type: {
      required: '年龄不能为空'
    },
    status: {
      required: '性别不能为空'
    }
  };
}
