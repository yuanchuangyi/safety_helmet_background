export class WorkMsg {
  title?: string;
  content?: string;
  createDate?: Date;
  createUserId?: number;
  id?: number;
  projectId?: number;
  status?: number;
  type?: number;
  url?: string;
  files?: any;
}
