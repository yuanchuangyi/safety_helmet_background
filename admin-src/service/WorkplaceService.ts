
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import {map, switchMapTo} from 'rxjs/operators';
import { environment } from '../environments/environment';
import { WorkProject } from '../../src/models/WorkProject';
import {CheckStatus, Company} from '../../src/models/Company';

@Injectable()
export class WorkplaceService {
  private baseUrl = environment.apiUrl + 'api';


  constructor(
    private http: HttpClient
  ) {
  }

  getAllWorkplaces(current: number = 1, pageSize: number = environment.pageSize, options: { status?: CheckStatus } = { status: CheckStatus.UNREVIEW })
    : Observable<{ success: boolean, errorMsg: string, res: WorkProject[], total: number }> {
    const paramsObj: any = {
      page: current,
      pageSize: pageSize,
      status: String(options.status),
    };
    const params = new HttpParams({ fromObject: paramsObj });
    return this.http.get<{ success: boolean, errorMsg: string, res: WorkProject[], total: number }>(`${this.baseUrl}/workproject/alllist`, { params });
  }

  getOneWorkplace(id): Observable<{res: WorkProject, success: boolean}> {
    return this.http.get<{res: WorkProject, success: boolean}>(`${this.baseUrl}/workproject/getone?id=${id}`);
  }

  reviewWorkplace(workplace: Array<WorkProject>): Observable<{res: WorkProject, success: boolean, errorMsg: string}> {
    return this.http.patch<{res: WorkProject, success: boolean, errorMsg: string}>(`${this.baseUrl}/workproject/review`, workplace);
  }

  deleteWorkplace(id): Observable<{res: boolean, success: boolean, errorMsg: string}> {
    return this.http.delete<{res: boolean, success: boolean, errorMsg: string}>(`${this.baseUrl}/workproject/del/${id}`);
  }
}
