import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { JobUser, TypeNameMap, statusType, StatusType } from '../../models/JobUser';
import { Workjob } from '../../models/Workjob';
import { WorkProject } from '../../models/WorkProject';
import { environment } from '../../environments/environment';
import { ConfirmDialogComponent } from '../confirmDialog/confirmDialog';
import { WorkerRealinfoComponent } from '../workerRealinfo/workerRealinfo';
import { WorkEvaluateComponent } from '../workEvaluate/workEvaluate';


import { WorkplaceService } from '../../service/WorkplaceService';
import { WorkjobService } from '../../service/WorkjobService';
import { JobuserService } from '../../service/jobuserService';
import { AuthGuardService } from '../../service/AuthGuardService';


@Component({
  templateUrl: './workerList.html',
  styleUrls: ['./workerList.css']
})
export class WorkerListComponent implements OnInit {
  workers: JobUser[];
  total: number;
  pageSize = environment.pageSize;
  page = 0;
  StatusType = StatusType;
  TypeNameMap = TypeNameMap;
  status = StatusType.All;
  workplaces: WorkProject[];
  workjobs: Workjob[];
  projectId: number;
  jobId: number;
  subscriptions: Subscription[] = [];
  selection: SelectionModel<JobUser>;
  cols: string[] = ['select', 'username', 'photo', 'sex', 'address', 'phone', 'email', 'qq', 'beginTime',  'status', 'operate'];

  constructor(
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private router: Router,
    private  workplaceService: WorkplaceService,
    private workjobService: WorkjobService,
    private jobuserService: JobuserService,
    private authGuardService: AuthGuardService
  ) {
  }

  ngOnInit() {
    this.workplaceService.getmylist().subscribe((res) => {
      this.workplaces = res.res;
      this.projectId = this.workplaces[0].id;
      const status = this.status;
      // const status = null;
      const projectId = this.projectId;
      const options = { projectId, status};
      this.workjobService.getalljob(this.page, this.pageSize, options).subscribe((result) => {
        this.workjobs = result.res;
        // this.jobId = this.workjobs[0].id;
        this.jobId = null;
        this.update({pageIndex: this.page, pageSize: this.pageSize});
      })
    });

    const initialSelection = [];
    const allowMultiSelect = true;
    this.selection = new SelectionModel<JobUser>(allowMultiSelect, initialSelection);
  }


  onProjectChange(projectId: number) {
    const status = this.status;
    const options: any = { projectId, status};
    this.workjobService.getalljob(this.page, this.pageSize, options).subscribe((res) => {
      this.workjobs = res.res;
      // if (this.workjobs[0]) {
      //   this.jobId = this.workjobs[0].id;
      // } else {
      //   this.jobId = null;
      // }
     this.jobId = null;
      this.projectId = projectId;
      this.update({pageIndex: this.page, pageSize: this.pageSize});
    })
  }


  onJobChange(jobid: number) {
    this.jobId = jobid;
    this.update({pageIndex: this.page, pageSize: this.pageSize});
  }


  onStatusChange(status: StatusType) {
    this.status = status;
    this.update({pageIndex: this.page, pageSize: this.pageSize});
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.workers.length;
    return numSelected === numRows;
  }

  allToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.workers.forEach(row => this.selection.select(row));
  }

  update(event: {pageIndex: number, pageSize: number}) {
    const jobId = this.jobId;
    const projectId = this.projectId;
    const status = this.status;
    const options: any = {jobId, projectId, status };
    this.jobuserService.getjobuser(event.pageIndex, event.pageSize, options).subscribe((res) => {
      this.workers = res.res;
      this.total = res.total;
      this.page = event.pageIndex;
    });
  }

  trackById(index: number, item: any) {
    return item.id;
  }


  onOpenRealinfo(worker) {
    const dialogRef = this.dialog.open( WorkerRealinfoComponent, {
      width: '500px',
      height: '420px',
      data: {
        worker: worker
      }
    });
    dialogRef.afterClosed().subscribe((res) => {
      this.update({pageIndex: this.page, pageSize: this.pageSize});
    });
  }


  onOpenEvaluation(worker) {
    const dialogRef = this.dialog.open(WorkEvaluateComponent, {
      width: '700px',
      height: '420px',
      data: {
        worker: worker,
        projectId: this.projectId
      }
    });
    dialogRef.afterClosed().subscribe((res) => {
      this.update({pageIndex: this.page, pageSize: this.pageSize});
    });
  }

  // onDelete(id) {
  //   this.authGuardService.checkLogin().pipe(filter(res => res)).subscribe(() => {
  //     const dialogRef = this.dialog.open(ConfirmDialogComponent, {
  //       width: '400px',
  //       height: '200px',
  //       data: {message: '确认删除？'}
  //     });
  //     dialogRef.afterClosed().subscribe((result) => {
  //       if (result) {
  //         this.userService.del(id).subscribe((res) => {
  //           if (res.success) {
  //             this.update({pageIndex: 0, pageSize: this.pageSize});
  //           }
  //         });
  //       }
  //     });
  //   });
  // }
  // onOpenEdit(worker) {
  //   this.authGuardService.checkLogin().pipe(filter(res => res)).subscribe(() => {
  //     const dialogRef = this.dialog.open(WorkerAddComponent, {
  //       width: '500px',
  //       height: '600px',
  //       data: {
  //         worker: worker
  //       }
  //     });
  //     dialogRef.afterClosed().subscribe((result) => {
  //       this.update({pageIndex: this.page, pageSize: this.pageSize});
  //     });
  //   });
  // }
  //
  // onOpenAdd() {
  //   this.authGuardService.checkLogin().pipe(filter(res => res)).subscribe(() => {
  //     const dialogRef = this.dialog.open(WorkerAddComponent, {
  //       width: '500px',
  //       height: '700px',
  //       data: {
  //       }
  //     });
  //     dialogRef.afterClosed().subscribe((result) => {
  //       this.update({pageIndex: this.page, pageSize: this.pageSize});
  //     });
  //   });
  // }
  //
}
