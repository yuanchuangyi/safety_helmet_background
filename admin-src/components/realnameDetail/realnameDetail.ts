import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { CheckStatus } from '../../../src/models/Company';
import { ConfirmDialogComponent } from '../../../src/components/confirmDialog/confirmDialog';
import { MatDialog, MatSnackBar } from '@angular/material';
import { UserRealinfo } from '../../../src/models/UserRealinfo';
import { RealinfoService } from '../../service/RealinfoService';

@Component({
  templateUrl: 'realnameDetail.html',
  styleUrls: ['realnameDetail.css']
})
export class RealnameDetailComponent implements OnInit {
  realinfoId: number;
  userinfo: UserRealinfo;
  userId: number;
  CheckStatus = CheckStatus;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private realinfoService: RealinfoService,
              private bar: MatSnackBar,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.userId = +this.route.snapshot.paramMap.get('id');
    this.userinfo = new UserRealinfo();
    this.realinfoService.getOneRealinfo(this.userId).subscribe((res) => {
      if (res.success) {
        this.userinfo = res.res;
        this.realinfoId = Number(this.userinfo.id);
      }
    })
  }

  onReview(status: CheckStatus) {
    this.userinfo.status = status;
    this.realinfoService.reviewRealinfo([{id: String(this.realinfoId), status, checkMsg: this.userinfo.checkMsg}]).subscribe((res) => {
      if (res.success) {
        this.onReturn(status);
      } else {
        this.bar.open(res.errorMsg);
      }
    })
  }


  onReturn(status: CheckStatus) {
    let path: string;
    if (status === CheckStatus.UNPASS) {
      path = '../../unpassed';
    } else if (status === CheckStatus.UNDERSHELF) {
      path = '../../undershelf';
    } else if (status === CheckStatus.UNREVIEW) {
      path = '../../unreview';
    } else {
      path = '../../published';
    }
    this.router.navigate([path], { relativeTo: this.route });
  }
}
