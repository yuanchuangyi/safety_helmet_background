import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ShareModule } from './share.module';

import { WorkerListComponent } from '../components/workerList/workerList';
import { WorkerAddComponent } from '../components/workerAdd/workerAdd';
import { WorkerRealinfoComponent } from '../components/workerRealinfo/workerRealinfo';
import { ApplyWorkerComponent } from '../components/applyWorker/applyWorker';
import { WorkEvaluateComponent } from '../components/workEvaluate/workEvaluate';
import { EvaluateAddComponent } from '../components/evaluateAdd/evaluateAdd';
import { WorkerRoutingModule } from '../router/worker';

import { UserService } from '../service/UserService';
import { UserinfoService } from '../service/UserinfoService';
import { UsersResolveService } from '../service/UsersResolveService';
import { JobuserService } from '../service/jobuserService';
import { WorkEvaluateService } from '../service/WorkEvaluateService';

@NgModule({
  imports: [
    BrowserModule,
    ShareModule,
    WorkerRoutingModule
  ],
  declarations: [
    WorkerListComponent,
    WorkerAddComponent,
    WorkerRealinfoComponent,
    ApplyWorkerComponent,
    WorkEvaluateComponent,
    EvaluateAddComponent
  ],
  providers: [
    UserService,
    UserinfoService,
    UsersResolveService,
    JobuserService,
    WorkEvaluateService
  ],
  entryComponents: [
    WorkerAddComponent,
    WorkerRealinfoComponent,
    ApplyWorkerComponent,
    WorkEvaluateComponent,
    EvaluateAddComponent
  ]
})
export class WorkerModule { }
