
import { Injectable } from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AuthService } from './AuthService';
import { User } from '../models/User';

@Injectable()
export class AuthGuardService implements CanActivate, CanActivateChild {

  constructor(private authService: AuthService, private router: Router) {}


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    // ActivatedRouteSnapshot包含了即将被激活的路由，而RouterStateSnapshot包含了该应用即将到达的状态。
    const url: string = state.url.slice(1);
    return this.checkLogin(url);
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(route, state);
  }

  checkLogin(url?: string): Observable<boolean> {
    const user = this.authService.user;
    if (user ) {
      return of(true);
    }
    return this.authService.getCurrent().pipe(map( res => {
        if (res.success) {
          this.authService.user = res.res;
          return true;
        } else {
          // Store the attempted URL for redirecting
          this.authService.redirectUrl = url;
          this.router.navigate(['/login']);
          return false;
        }
      }),
      catchError((err) => {
        console.log(err);
        this.router.navigate(['']);
        return of(false);
      }));
  }

  checkWorkertop(): Observable<boolean> {
    if ( this.authService.user ) {
      return of(this.authService.user.workerType === '2');
    } else {
      return this.authService.userObservable.pipe(map((user: User) => {
        return user && user.workerType === '2';
      }));
    }
  }
}
