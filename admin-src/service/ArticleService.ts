
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import {map, switchMapTo} from 'rxjs/operators';
import { environment } from '../environments/environment';
import { Article } from '../../src/models/Article';
import { CheckStatus } from '../../src/models/Company';
import { User } from '../../src/models/User';

@Injectable()
export class ArticleService {
  private baseUrl = environment.apiUrl + 'api';


  constructor(
    private http: HttpClient
  ) {
  }

  getAllArticles(current: number = 1, pageSize: number = environment.pageSize, options: { status?: CheckStatus } = { status: CheckStatus.UNREVIEW })
    : Observable<{ success: boolean, errorMsg: string, res: Article[], total: number }> {
    const paramsObj: any = {
      status: String(options.status),
      page: current,
      pageSize: pageSize
    };
    const params = new HttpParams({ fromObject: paramsObj });
    return this.http.get<{ success: boolean, errorMsg: string, res: Article[], total: number }>(`${this.baseUrl}/cmsarticle/list`, { params });
  }

  getOneArticle(id): Observable<{res: Article, success: boolean, errorMsg: string, user: User}> {
    return this.http.get<{res: Article, success: boolean, errorMsg: string, user: User}>(`${this.baseUrl}/cmsarticle/getone/${id}`);
  }

  reviewArticle(article: Array<Article>): Observable<{res: Article, success: boolean, errorMsg: string}> {
    return this.http.patch<{res: Article, success: boolean, errorMsg: string}>(`${this.baseUrl}/cmsarticle/review`, article);
  }

  deleteArticle(id): Observable<{res: boolean, success: boolean, errorMsg: string}> {
    return this.http.delete<{res: boolean, success: boolean, errorMsg: string}>(`${this.baseUrl}/cmsarticle/del/${id}`);
  }
}
