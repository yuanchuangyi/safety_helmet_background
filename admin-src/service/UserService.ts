
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import {map, switchMapTo} from 'rxjs/operators';
import { environment } from '../environments/environment';

import { User } from '../../src/models/User';

@Injectable()
export class UserService {
  private baseUrl = environment.apiUrl + 'api/sysuser';
  private smsUrl = environment.apiUrl + 'api/sms';

  constructor(
    private http: HttpClient
  ) {
  }

  getAllUsers(): Observable<{res: User[], success: boolean, total: number}> {
    return this.http.get<{res: User[], success: boolean, total: number}>(`${this.baseUrl}/getlist`);
  }

  getUsers(current: number = 0, pageSize: number = environment.pageSize): Observable<{res: User[], success: boolean, total: number}> {
    let params: HttpParams = new HttpParams();
    params = params.append('page', String(current + 1));
    params = params.append('pageSize', String(pageSize));
    return this.http.post<{res: User[], success: boolean, total: number}>(`${this.baseUrl}/getlist`, { params : params});
  }

  getUser(id: string): Observable<{res: User, success: boolean}> {
    return this.http.get<{res: User, success: boolean}>( `${this.baseUrl}/getone/${id}`);
  }

  update(user: User): Observable<{res: User, success: boolean}> {
    delete user.createDate;
    delete user.updateDate;
    const url = `${this.baseUrl}/update`;
    return this.http
      .patch<{res: User, success: boolean}>(url, user);
  }
  changePassword(user: User): Observable<{res: User, success: boolean, errorMsg: string}>{
    return this.http.patch<{ res: User, success: boolean, errorMsg: string }>(`${this.baseUrl}/changepassword`, user);
  }
  register(user: User, code: string): Observable<{success: boolean, res: User, errorMsg: string}> {
    return this.http
      .post<{success: boolean, res: User, errorMsg: string}>(`${this.baseUrl}/insert/${code}`, user);
  }

  isExist(username: string): Observable<{success: boolean}> {
    return this.http
      .get<{success: boolean}>(`${this.baseUrl}/checkusername?username=${username}`);
  }

  checkPhone(phone): Observable<{res: boolean, success: boolean}> {
    return this.http.get<{res: boolean, success: boolean}>(`${this.baseUrl}/checkphonehasreg?phone=${phone}`);
  }

  bindPhone(phone): Observable<{res: string, success: boolean, errorMsg: string}> {
    return this.http.get<{res: string, success: boolean, errorMsg: string}>(`${this.smsUrl}/bindphone/${phone}`);
  }

  checkcode(code): Observable<{res: string[], success: boolean, errorMsg: string}> {
    return this.http.get<{res: string[], success: boolean, errorMsg: string}>(`${this.smsUrl}/checkcode/${code}`);
  }
  del(id: number): Observable<{success: boolean}> {
    const url = `${this.baseUrl}/del/${id}`;
    return this.http.delete<{success: boolean}>(url);
  }


}
