import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxPaginationModule } from 'ngx-pagination';
import { MatToolbarModule, MatButtonModule, MatTableModule, MatCheckboxModule, MatDialogModule,
  MatFormFieldModule, MatInputModule, MatPaginatorModule, MatPaginatorIntl,
  MatMenuModule, MatSelectModule, MatDatepickerModule, MAT_DATE_LOCALE, MAT_DATE_FORMATS, DateAdapter,
} from '@angular/material';
import { MatMomentDateModule, MomentDateAdapter } from '@angular/material-moment-adapter';
import { AppSelectComponent } from '../../src/components/appSelect/appSelect';
import { AppOptionComponent } from '../../src/components/appSelect/appOption';
import { ConfirmDialogComponent } from '../../src/components/confirmDialog/confirmDialog';
import { PaginationComponent } from '../components/pagination/pagination';
import { StatusNavComponent } from '../components/statusNav/statusNav';
import { CheckBoxComponent } from '../components/checkBox/checkBox';

import { MatPaginatorIntlService } from '../../src/service/MatPaginatorIntlService';
import { myFormats } from '../environments/environment';
import { MatTreeModule } from '@angular/material/tree';
import { MatIconModule } from '@angular/material';
import { FileUploadModule } from 'ng2-file-upload';
import { HeaderComponent } from '../components/header/header';

@NgModule({
  imports: [
    CommonModule, FormsModule,
    NgxPaginationModule, RouterModule,
    BrowserAnimationsModule, MatToolbarModule,
    MatButtonModule, MatTableModule,
    MatCheckboxModule, MatDialogModule,
    MatFormFieldModule, MatInputModule,
    MatPaginatorModule, MatMenuModule,
    MatSelectModule, MatDatepickerModule,
    MatMomentDateModule, MatTreeModule, MatIconModule, ReactiveFormsModule, FileUploadModule
  ],
  declarations: [
    AppSelectComponent,
    AppOptionComponent,
    ConfirmDialogComponent,
    PaginationComponent,
    StatusNavComponent,
    CheckBoxComponent,
    HeaderComponent
  ],
  providers: [
    { provide: MatPaginatorIntl, useClass: MatPaginatorIntlService},
    {provide: MAT_DATE_LOCALE, useValue: 'zh-cn'},
    {provide: MAT_DATE_FORMATS, useValue: myFormats},
    { provide: DateAdapter, useClass: MomentDateAdapter },
  ],
  entryComponents: [
    ConfirmDialogComponent
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatMenuModule,
    MatSelectModule,
    MatDatepickerModule,
    MatMomentDateModule,
    AppSelectComponent,
    AppOptionComponent,
    PaginationComponent,
    MatTreeModule,
    MatIconModule,
    FileUploadModule,
    StatusNavComponent,
    CheckBoxComponent,
    NgxPaginationModule,
    HeaderComponent
  ]
})
export class ShareModule {}
