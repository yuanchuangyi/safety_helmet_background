import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WorkerTopComponent } from '../components/workerTop/workerTop';
import { AuthGuardService } from '../service/AuthGuardService';
import { WorkertopResolveService } from '../service/WorkertopResolveService';
import { AddMessageComponent } from '../components/addMessage/addMessage';

const routes: Routes = [
  {
    path: 'workerTop',
    canActivate: [AuthGuardService],
    children: [
      {
        path: '',
        component: WorkerTopComponent,
        resolve: {
          result: WorkertopResolveService
        }
      }
    ]

  }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class WorkerTopRoutingModule {}
