import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { CheckStatus } from '../../../src/models/Company';
import { ConfirmDialogComponent } from '../../../src/components/confirmDialog/confirmDialog';
import { MatDialog, MatSnackBar } from '@angular/material';
import { WorkProject } from '../../../src/models/WorkProject';
import { WorkplaceService } from '../../service/WorkplaceService';

@Component({
  templateUrl: 'workplaceDetail.html',
  styleUrls: ['workplaceDetail.css']
})
export class WorkplaceDetailComponent implements OnInit {
  projectId: number;
  workplace: WorkProject;
  CheckStatus = CheckStatus;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private  workplaceService: WorkplaceService,
              private bar: MatSnackBar,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.projectId = +this.route.snapshot.paramMap.get('id');
    this.workplace = new WorkProject();
    this.workplaceService.getOneWorkplace(this.projectId).subscribe((res) => {
      if (res.success) {
        this.workplace = res.res;
      }
    })
  }

  onReview(status: CheckStatus) {
    this.workplace.status = status;
    this.workplaceService.reviewWorkplace([{id: this.projectId, status, checkMsg: this.workplace.checkMsg}]).subscribe((res) => {
      if (res.success) {
        this.onReturn(status);
      } else {
        this.bar.open(res.errorMsg);
      }
    })
  }

  onDelete() {
    this.workplaceService.deleteWorkplace(this.projectId).subscribe((res) => {
      if (res.success) {
        alert('删除成功');
        // this.onReturn(this.company.status);
      } else {
        this.bar.open(res.errorMsg);
      }
    })
  }

  onReturn(status: CheckStatus) {
    let path: string;
    if (status === CheckStatus.UNPASS) {
      path = '../../unpassed';
    } else if (status === CheckStatus.UNDERSHELF) {
      path = '../../undershelf';
    } else if (status === CheckStatus.UNREVIEW) {
      path = '../../unreview';
    } else {
      path = '../../published';
    }
    this.router.navigate([path], { relativeTo: this.route });
  }


}
