import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MessageManageComponent } from '../components/messageManage/messageManage';
import { AuthGuardService } from '../service/AuthGuardService';
import { MessageResolveService } from '../service/MessageResolveService';
import { EditorDeactivateGuardService} from '../service/EditorDeactivateGuardService';
import { AddMessageComponent } from '../components/addMessage/addMessage';

const routes: Routes = [
  {
    path: 'messageManage',
    canActivate: [AuthGuardService],
    children: [
      {
        path: '',
        component: MessageManageComponent,
        resolve: {
          result: MessageResolveService
        }
      },
      {
        path: 'new-message',
        component: AddMessageComponent,
        resolve: {
          message: MessageResolveService
        },
        canDeactivate: [ EditorDeactivateGuardService ]
      }
    ]

  }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class MessageManageRoutingModule {}
