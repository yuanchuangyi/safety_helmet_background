import { Component, OnInit } from '@angular/core';

import { Video } from '../../../src/models/Video';
import { VideoService } from '../../service/VideoService';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../src/environments/environment';
import { tap } from 'rxjs/operators';
import { MatDialog, MatSnackBar } from '@angular/material';

import { CheckStatus,  CheckStatusMap} from  '../../../src/models/Company';
import { adjustList, trackById } from '../../../src/others/utils';

@Component({
  templateUrl: './videoManage.html',
  styleUrls: ['./videoManage.css']
})
export class VideoManageComponent implements OnInit {

  CheckStatus = CheckStatus;
  CheckStatusMap = CheckStatusMap;
  status: CheckStatus;
  videos: Video[];
  total: number;
  currentPage: number = 1;
  pageSize: number = environment.adminPageSize;
  trackById = trackById;
  environment = environment;
  constructor(
    private videoService: VideoService,
    private route: ActivatedRoute,
    private router: Router,
    private bar: MatSnackBar,
    private dialog: MatDialog,
  ) {

  }

  ngOnInit() {
    this.route.data.subscribe((data: {result: {res: Video[], total: number}}) => {
      this.videos = adjustList(data.result.res, 4);
      // this.companys = data.result.res;
      this.total = data.result.total;
    });

    const url = this.router.routerState.snapshot.url;
    if (url.indexOf('published') > 0) {
      this.status = CheckStatus.PASS;
    } else if (url.indexOf('unpassed') > 0) {
      this.status = CheckStatus.UNPASS;
    } else if (url.indexOf('undershelf') > 0) {
      this.status = CheckStatus.UNDERSHELF;
    } else  {
      this.status = CheckStatus.UNREVIEW;
    }
  }

  onPageChanged(page: number) {
    this.update(page, this.status).subscribe();
  }


  update(page: number, status?: CheckStatus) {
    const options: any = {  status };
    return this.videoService.getAllVideos(page, this.pageSize, options).pipe(tap((res) => {
      if (res.success) {
        this.currentPage = page;
        this.total = res.total;
        // this.companys = res.res;
        this.videos = adjustList(res.res, 4);
      } else {
        this.bar.open(res.errorMsg);
      }
    }));
  }

}
