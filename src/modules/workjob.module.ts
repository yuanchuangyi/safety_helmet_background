import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ShareModule } from './share.module';
import { WorkjobRoutingModule } from '../router/workjob';
import { WorkJobComponent } from '../components/workJob/workJob';
// import { WorkjobResolveService } from '../service/WorkjobResolveService';
import { WorkjobService } from '../service/WorkjobService';

@NgModule({
  imports: [
    BrowserModule,
    ShareModule,
    WorkjobRoutingModule
  ],
  declarations: [
    WorkJobComponent
  ],
  providers: [
    // WorkjobResolveService,
    WorkjobService
  ],
  entryComponents: [

  ]
})
export class WorkjobModule { }
