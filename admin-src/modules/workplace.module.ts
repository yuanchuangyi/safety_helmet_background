import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ShareModule } from './share.module';
import { WorkplaceManageRoutingModule } from '../router/workplaceManage';
import { WorkplaceManageComponent } from '../components/workplaceManage/workplaceManage';
import { WorkplaceService } from '../service/WorkplaceService';
import { WorkplaceResolveService } from '../service/WorkplaceResolveService';
import { WorkplaceDetailComponent } from '../components/workplaceDetail/workplaceDetail';

@NgModule({
  imports: [BrowserModule, ShareModule, WorkplaceManageRoutingModule],
  declarations: [
    WorkplaceManageComponent,
    WorkplaceDetailComponent
  ],
  providers: [
    WorkplaceService,
    WorkplaceResolveService
  ]
})
export class WorkplaceModule { }
