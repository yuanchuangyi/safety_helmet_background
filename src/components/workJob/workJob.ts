import { Component, OnInit, ViewChild, Inject, } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {MatDialogRef, MAT_DIALOG_DATA, MatDatepickerInputEvent, MatDialog} from '@angular/material';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { WorkProject } from '../../models/WorkProject';
import { Workjob } from '../../models/Workjob';

import { WorkjobService } from '../../service/WorkjobService';
import { WorkplaceService } from '../../service/WorkplaceService';
import { AuthService } from '../../service/AuthService';

import { PostJobComponent } from '../postJob/postJob';
import { ConfirmDialogComponent } from '../confirmDialog/confirmDialog';
import { ApplyWorkerComponent } from '../applyWorker/applyWorker';

@Component({
  templateUrl: './workJob.html',
  styleUrls: ['./workJob.css']
})
export class WorkJobComponent implements OnInit {

  selection: SelectionModel<Workjob>;
  workjobs: Workjob[];
  total: number;
  subscriptions: Subscription[] = [];
  pageSize = environment.pageSize;
  page = 0;
  workplace: WorkProject;
  projectId: number;
  status: number = 1;
  createUserId: string;
  haveJob: boolean = true;
  cols: string[] = ['select', 'jobName', 'description', 'salary', 'peopleNum', 'workAge', 'status'];
  constructor(
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<WorkJobComponent>,
    private route: ActivatedRoute,
    private router: Router,
    private workjobService: WorkjobService,
    private workplaceService: WorkplaceService,
    private authService: AuthService,
    @Inject(MAT_DIALOG_DATA) public data: {workplace: WorkProject}
  ) {
    this.workplace = data.workplace;
  }

  ngOnInit() {
    this.createUserId = String(this.authService.user.id);
    this.projectId = this.workplace.id;
    this.cols = ['select', 'jobName', 'description', 'salary', 'peopleNum', 'workAge', 'status', 'operate'];
    this.update(this.page, this.projectId, this.status).subscribe();
    const initialSelection = [];
    const allowMultiSelect = true;
    this.selection = new SelectionModel<Workjob>(allowMultiSelect, initialSelection);

  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.workjobs.length;
    return numSelected === numRows;
  }

  allToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.workjobs.forEach(row => this.selection.select(row));
  }


  onDelete(id) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      height: '200px',
      data: {message: '确认删除？'}
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.workjobService.deleteJob(id).subscribe((res) => {
          if (res.success) {
           this.update(1, this.projectId, this.status).subscribe();
          }
        });
      }
    });
  }

  onOpenEdit(workjob) {
    const dialogRef = this.dialog.open( PostJobComponent, {
      width: '500px',
      height: '600px',
      data: {
        workjob: workjob
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.update(this.page, this.projectId, this.status).subscribe();
    });
  }

  onOpenWorkers(workjob) {
    const dialogRef = this.dialog.open( ApplyWorkerComponent, {
      width: '700px',
      height: '400px',
      data: {
        workjob: workjob,
        projectid: this.projectId
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.update(this.page, this.projectId, this.status);
    });
  }

  update(page: number, projectId?: number, status?: number) {
    const options: any = {  projectId , status};
    return this.workjobService.getalljob(page, this.pageSize, options).pipe(tap((res) => {
      if (res.success) {
        this.page = page;
        this.total = res.total;
        this.workjobs = res.res;
        if (this.total === 0) {
          this.haveJob = false;
        }
      }
    }));
  }

  // update(page: number) {
  //   return this.workjobService.getmyjob(page, this.pageSize, this.createUserId,).pipe(tap((res) => {
  //     if (res.success) {
  //       this.page = page;
  //       this.total = res.total;
  //       this.workjobs = res.res;
  //     }
  //   }));
  // }

  onPageChanged(page: number) {
    this.update(page, this.projectId, this.status).subscribe();
  }

  trackById(index: number, item: any) {
    return item.id;
  }

}
