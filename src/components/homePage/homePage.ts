import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../../service/CompanyService';
import { Company } from '../../models/Company';
import {FormControl} from '@angular/forms';
import { Moment } from 'moment';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './homePage.html',
  styleUrls: ['./homePage.css'],
})
export class HomePageComponent implements OnInit {
  hasCompany: boolean = false;
  company: Company;
  status: number = 2;
  constructor(
    private companyService: CompanyService,
  ) {
  }
  ngOnInit() {
    this.companyService.getCompany(this.status).subscribe((res) => {
      if (res.res.length === 0) {
        this.hasCompany = false;
      } else {
        this.hasCompany = true;
        this.company = res.res[0];
        this.company.createDateShow = new FormControl(moment(this.company.createDate));
        this.company.createDate = moment(this.company.createDate).toDate();
      }
    })
  }
}
