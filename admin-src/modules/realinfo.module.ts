import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ShareModule } from './share.module';
import { RealinfoRoutingModule } from '../router/realinfo';

import { RealinfoService} from '../service/RealinfoService';
import { RealinfoResolveService } from '../service/RealinfoResolveService';

import { RealnameManageComponent } from '../components/realnameManage/realnameManage';
import { RealnameDetailComponent } from '../components/realnameDetail/realnameDetail';

@NgModule({
  imports: [BrowserModule, ShareModule, RealinfoRoutingModule],
  declarations: [
    RealnameManageComponent,
    RealnameDetailComponent
  ],
  providers: [
    RealinfoService,
    RealinfoResolveService
  ]
})
export class RealinfoModule { }
