import {AfterViewChecked, Component, OnInit, ViewChild} from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '../../models/User';
import { AuthService } from '../../service/AuthService';
import { UserService } from '../../service/UserService';
import { UserinfoService } from '../../service/UserinfoService';
import { CheckStatus, CheckStatusMap } from '../../models/Company';
import { Router, NavigationExtras } from '@angular/router';
@Component({
  templateUrl: './personInfo.html',
  styleUrls: ['./personInfo.css']
})
export class  PersonInfoComponent implements OnInit, AfterViewChecked {
  user: User = new User();
  messages = {
    username: '',
    nickname: '',
    phone: '',
    address: '',
    signature: ''
  };
  checkStatus = CheckStatus;
  checkStatusMap = CheckStatusMap;
  realinfo: boolean;
  status: number;
  constructor(
    private  authService: AuthService,
    private  userService: UserService,
    private  userinfoService: UserinfoService,
    public router: Router
  ) {}
  @ViewChild('userForm')
  userForm: NgForm;

  ngAfterViewChecked() {
    if (this.userForm) {
      this.userForm.valueChanges.subscribe(data => this.checkValidate());
    }
  }
  ngOnInit() {
    this.user = this.authService.user;
    this.authService.userObservable.subscribe((user: User) => {
      this.user = user;
    });
    this.userinfoService.getoneInfo(this.user.id).subscribe((res) => {
        this.status = res.res.status;
    })
  }

  lookRealinfo() {
    this.router.navigate(['/realnameProve'], {queryParams: {'userid': this.user.id}});
  }
  onSubmit() {
    this.checkValidate(true);
    if (this.userForm.invalid) {
      // 触发mat的error
      this.userForm.onSubmit(null);
      return false;
    }
    if (this.user.id !== undefined) {
      this.userService.update(this.user).subscribe((res) => {
          alert('修改成功!');
      });
    }
  }


  checkValidate(isSubmit?: boolean) {
    const form = this.userForm;

    for (const field in PersonInfoComponent.ErrorMessages) {

      const control = form.controls[field];

      if (control && control.valid) {
        this.messages[field] = '';
      }
      if (control && (control.dirty || isSubmit) && !control.valid) {
        const messages = PersonInfoComponent.ErrorMessages[field];
        if (control.errors) {
          this.messages[field] = messages[Object.keys(control.errors)[0]];
        }
      }

    }
  }

  static ErrorMessages = {
    username: {
      required: '用户名不能为空',
      pattern: '以字母开头，由4-20个字母、数字和"_"组成',
    },
    nickname: {

      required: '昵称不能为空'
    },
    phone: {
      pattern: '手机号格式不正确',
      required: '手机号不能为空'
    },
    address: {
      required: '籍贯不能为空'
    },
    signature: {
      required: '签名不能为空'
    }
  };
}
