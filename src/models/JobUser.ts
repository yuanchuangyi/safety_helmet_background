import { Company } from './Company';
import { User } from './User';
import { WorkProject } from './WorkProject';
import { Workjob } from './Workjob';

export interface JobUser {
  checkDate?: Date;
  companyId?: number;
  createDate?: Date;
  id?: number;
  jobId?: number;
  leaderMsg?: string;
  projectId?: number;
  remark?: string;
  status?: StatusType;
  sysCompany?: Company;
  sysUser?: User;
  updateDate?: Date;
  userId?: number;
  workProject?: WorkProject;
  workProjectJob?: Workjob;
  workmanMsg?: string;
}
export enum StatusType {
  All = 0,
  Applying = 1,
  Interview = 2,
  Entry = 3,
  Reject = 4,
  Leave = 5
}

export const statusType = [StatusType.All, StatusType.Applying, StatusType.Interview, StatusType.Entry, StatusType.Reject, StatusType.Leave];
export const TypeNameMap = {
  [StatusType.All]: '全部',
  [StatusType.Applying] : '申请中',
  [StatusType.Interview]: '通知面试',
  [StatusType.Entry]: '入职',
  [StatusType.Reject]: '拒绝',
  [StatusType.Leave]: '离职'
}

