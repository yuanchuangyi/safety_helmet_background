import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ShareModule } from './share.module';
import { WorkplaceComponent } from '../components/workplace/workplace';
import { WorkplaceAddComponent } from '../components/workplaceAdd/workplaceAdd';
import { PostJobComponent } from '../components/postJob/postJob';
import { WorkmsgComponent } from '../components/workmsg/workmsg';
import { MsgAddComponent } from '../components/msgAdd/msgAdd';
import { WorkplaceMapComponent } from '../components/workplaceMap/workplaceMap';

import { WorkplaceRoutingModule } from '../router/workplace';
import { WorkplaceService } from '../service/WorkplaceService';
import { WorkjobService } from '../service/WorkjobService';
import { WorkplaceResolveService } from '../service/WorkplaceResolveService';
import { WorkmsgService } from '../service/WorkmsgService';

@NgModule({
  imports: [
    BrowserModule,
    ShareModule,
    WorkplaceRoutingModule
  ],
  declarations: [
    WorkplaceComponent,
    WorkplaceAddComponent,
    PostJobComponent,
    WorkmsgComponent,
    MsgAddComponent,
    WorkplaceMapComponent
  ],
  providers: [
    WorkplaceService,
    WorkplaceResolveService,
    WorkjobService,
    WorkmsgService
  ],
  entryComponents: [
    WorkplaceAddComponent,
    PostJobComponent,
    WorkmsgComponent,
    MsgAddComponent,
    WorkplaceMapComponent
  ]
})
export class WorkplaceModule { }
