import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuardService } from '../service/AuthGuardService';
import { WorkplaceResolveService } from '../service/WorkplaceResolveService';
import { WorklogComponent } from '../components/worklog/worklog';

const routes: Routes = [
  {
    path: 'worklog',
    canActivate: [AuthGuardService],
    component: WorklogComponent
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ],
})
export class WorklogRoutingModule {}
