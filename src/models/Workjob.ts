
export interface Workjob {
  companyId?: number;
  createUserid?: number;
  description?: string;
  id?: number;
  jobName?: string;
  peopleNum?: number;
  projectId?: number;
  projectName?: string;
  salary?: string;
  status?: number;
  workAge?: number;
  createDate?: Date;
  updateDate?: Date;
}
