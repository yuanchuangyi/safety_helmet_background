import { NgModule } from '@angular/core';
import { ShareModule } from './share.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AuthGuardService } from '../service/AuthGuardService';
import { AuthService } from '../service/AuthService';
import { UserService } from '../service/UserService';

import { LoginComponent } from '../components/login/login';
import { RegisterComponent } from '../components/register/register';
import { RealnameProveComponent } from '../components/realnameProve/realnameProve';
import { PersonInfoComponent } from '../components/personInfo/personInfo';
import { SecurityComponent } from '../components/security/security';
import { ForgetPasswordComponent } from '../components/forgetPassword/forgetPassword';
import { ChangePasswordComponent } from '../components/changePassword/changePassword';
import { FileUploadModule } from 'ng2-file-upload';
@NgModule({
  imports: [
    ShareModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    RealnameProveComponent,
    PersonInfoComponent,
    SecurityComponent,
    ForgetPasswordComponent,
    ChangePasswordComponent
  ],
  providers: [
    AuthGuardService,
    AuthService,
    UserService
  ],
  exports: [
    LoginComponent,
    RegisterComponent,
    RealnameProveComponent,
    PersonInfoComponent,
    SecurityComponent
  ]
})
export class AuthModule {}
