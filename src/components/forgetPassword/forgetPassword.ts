import {AfterViewChecked, Component, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { User } from '../../models/User';
import { NgForm } from '@angular/forms';
import {RegisterComponent} from '../register/register';
import {interval} from 'rxjs';
import { UserService } from '../../service/UserService';
import { Router, NavigationExtras } from '@angular/router';
@Component({
  templateUrl: './forgetPassword.html',
  styleUrls: ['./forgetPassword.css'],
})
export class ForgetPasswordComponent implements AfterViewChecked  {

  user: User;
  messages = {
    phone: '',
    keyWord: ''
  };
  timeNum = 0;
  @ViewChild('passwordForm')
  passwordForm: NgForm;

  ngAfterViewChecked() {
    // Angular的“单向数据流”规则禁止在一个视图已经被组合好之后再更新视图.而此时已组合好
    if (this.passwordForm) {
      this.passwordForm.valueChanges.subscribe(data => this.checkValidate());
    }
  }
  constructor(private userService: UserService, public router: Router) {
        this.user = new User();
  }


  onGetPhonecode(phone) {
    this.messages.keyWord = '';
    const phoneControl = this.passwordForm.controls['phone'];
    if (phoneControl.invalid) {
      this.messages.phone = ForgetPasswordComponent.ErrorMessages.phone[Object.keys(phoneControl.errors)[0]];
      return false;
    }

    this.userService.changePass(phone).subscribe((res) => {
      if (!res.success) {
        this.messages.phone = res.errorMsg;
      } else {
        const sub = interval(1000).subscribe((x) => {
          this.timeNum = 60 - x;
          if (this.timeNum <= 0) {
            this.timeNum = 0;
            sub.unsubscribe();
          }
        });
      }
    })
  }

  setNewcode() {
    this.checkValidate(true);
    if (this.passwordForm.invalid) {
      return false;
    }
    this.router.navigate(['/changePassword'], {queryParams: {'keyWord': this.user.keyWord}});
  }

  checkValidate(isSubmit?: boolean) {
    const form = this.passwordForm;

    for (const field in ForgetPasswordComponent.ErrorMessages) {

      const control = form.controls[field];

      if (control && control.valid) {
        this.messages[field] = '';
      }
      if (control && (control.dirty || isSubmit) && !control.valid) {
        const messages = ForgetPasswordComponent.ErrorMessages[field];
        if (control.errors) {
          this.messages[field] = messages[Object.keys(control.errors)[0]];
        }
      }

    }
  }

  static ErrorMessages = {
    phone: {
      required: '手机号不能为空',
      hasExist: '手机号已存在',
      pattern: '手机号格式不正确'
    },
    keyWord: {
      required: '验证码不能为空',
      error: '验证码错误11'
    },
  };
}
