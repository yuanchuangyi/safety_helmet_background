import { Component, OnInit, Inject } from '@angular/core';
import { Company } from '../../models/Company';
import { WorkplaceService } from '../../service/WorkplaceService';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { WorkplaceAddComponent } from '../workplaceAdd/workplaceAdd';
import { WorkProject } from '../../models/WorkProject';
declare var  AMap: any;    // 一定要声明AMap，要不然报错找不到AMap
@Component({
  templateUrl: './workplaceMap.html',
  styleUrls: ['./workplaceMap.css']
})
export class WorkplaceMapComponent implements OnInit {
  workplace: WorkProject;
  longitude: Number;
  latitude: Number;
  constructor(
    private dialogRef: MatDialogRef<WorkplaceMapComponent>,
    private workplaceService: WorkplaceService,
    @Inject(MAT_DIALOG_DATA) public data: { workplace: WorkProject }
  ) {
    this.workplace = data.workplace;
  }
  ngOnInit() {
    this.getMap ();
  }
  // 地图要放到函数里。
  getMap() {
    this.longitude = this.workplace.longitude;
    this.latitude = this.workplace.latitude;
    const map = new AMap.Map('container', {
      resizeEnable: true,
      zoom: 11,
      center: [this.longitude, this.latitude]
    });

    const customMarker = new AMap.Marker({
      // icon: new AMap.Icon({
      //   size: new AMap.Size(27, 36),
      //   imageOffset: new AMap.Pixel(0, -60),
      //   image: 'http://webapi.amap.com/theme/v1.3/markers/n/mark_b.png',
      //   position: new AMap.LngLat(this.longitude, this.latitude)
      // }),
      position: new AMap.LngLat(this.longitude, this.latitude),
      icon:  'http://webapi.amap.com/theme/v1.3/markers/n/mark_b.png',
      offset: new AMap.Pixel(-13, -30)
    });
    customMarker.setMap(map);
    customMarker.setLabel({
      content: this.workplace.projectName,
      position: new AMap.LngLat(this.longitude, this.latitude),
      offset: new AMap.Pixel(20, 20),
    })
  }

}
