import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, Validators, ValidatorFn } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

import { User } from '../../models/User';
import { AuthService } from '../../service/AuthService';
import { UserService } from '../../service/UserService';

@Component({
  templateUrl: './security.html',
  styleUrls: ['./security.css'],
})
export class SecurityComponent implements OnInit {

  static ErrorMessages = {
    password: {
      required: '密码不能为空'
    },
    newPassword: {
      required: '密码不能为空',
      pattern: '长度为6-12位，必须由数字，字母共同组成'
    },
    confirmPassword: {
      required: '确认密码不能为空',
      same: '两次密码不一样'
    }
  };

  messages = {
    password: '',
    newPassword: '',
    confirmPassword: ''
  };
  user: User;
  userForm: FormGroup;

  constructor (
    private authService: AuthService,
    private userService: UserService,
    private fb: FormBuilder,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.user = this.authService.user;
    this.userForm = this.fb.group({
      password: ['', Validators.required],
      newPassword: ['', [Validators.required, Validators.pattern(/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,12}$/)]],
      confirmPassword: ['', [Validators.required, this.checkConfirmPassword('newPassword')]]
    });

    this.userForm.valueChanges.subscribe(_ => this.checkValidate());
    this.userForm.get('newPassword').valueChanges.subscribe(() => {
      this.userForm.get('confirmPassword').updateValueAndValidity();
    });
  }

  onSubmit() {
    this.checkValidate(true);
    if (this.userForm.invalid) {
      return false;
    }
    this.userService.changePassword(this.userForm.value).subscribe((res) => {
      if (res.success) {
        alert('修改成功');
        this.userForm.reset();
      } else {
        this.messages.password = res.errorMsg;
      }
    });
  }



  checkConfirmPassword(controlName: string): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
      const isSame = !!control.parent &&
        (!control.parent.get(controlName).value
          || control.parent.get(controlName).value === control.value);
      return isSame ? null : { 'same': true } ;
    };
  }

  checkValidate(isSubmit?: boolean) {
    const form = this.userForm;
    Object.keys(SecurityComponent.ErrorMessages).forEach((field) => {
      const control = form.get(field);

      if (control && control.valid) {
        this.messages[field] = '';
      }
      if (control && (control.dirty || isSubmit) && !control.valid) {
        const messages = SecurityComponent.ErrorMessages[field];
        if (control.errors) {
          this.messages[field] = messages[Object.keys(control.errors)[0]];
        }
      }

    });
  }

}
