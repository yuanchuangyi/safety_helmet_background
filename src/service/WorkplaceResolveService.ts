
import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { WorkplaceService } from './WorkplaceService';
import { WorkProject } from '../models/WorkProject';

@Injectable()
export class WorkplaceResolveService implements Resolve<{total: number, workplaces: WorkProject[]}> {

  constructor(private workplaceService: WorkplaceService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<{total: number, workplaces: WorkProject[]}>  {

    return this.workplaceService.getmylist().pipe(map(res => {
      if (res.success) {
        return  {total: res.total, workplaces: res.res};
      } else {
        console.error(res.res);
        return {total: 0, workplaces: []};
      }
    }));
  }
}
