import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticleListComponent } from '../components/articleList/articleList';
import { AuthGuardService } from '../service/AuthGuardService';
import { ArticleResolveService } from '../service/ArticleResolveService';
import { WorkplaceDetailComponent } from '../components/workplaceDetail/workplaceDetail';
import { ArticleDetailComponent } from '../components/articleDetail/articleDetail';

const routes: Routes = [
  {
    path: 'articleManage',
    canActivate: [AuthGuardService],
    children: [
      {
        path: '',
        redirectTo: 'unreview',
        pathMatch: 'full'
      },
      {
        path: 'unreview',
        component: ArticleListComponent,
        resolve: {
          result: ArticleResolveService
        }
      },
      {
        path: 'published',
        component: ArticleListComponent,
        resolve: {
          result: ArticleResolveService
        }
      },
      {
        path: 'unpassed',
        component: ArticleListComponent,
        resolve: {
          result: ArticleResolveService
        }
      },
      {
        path: 'undershelf',
        component: ArticleListComponent,
        resolve: {
          result: ArticleResolveService
        }
      },
      {
        path: 'articleDetail/:id',
        component: ArticleDetailComponent,
        resolve: {
          result: ArticleResolveService
        }
      }
    ]
  }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class ArticleRoutingModule {}
