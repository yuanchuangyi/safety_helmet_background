import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Article } from '../../src/models/Article';
import { ArticleService } from './ArticleService';

import { environment } from '../../src/environments/environment';
import { CheckStatus } from '../../src/models/Company';

@Injectable()
export class ArticleResolveService implements Resolve<{ res: Article[], total: number}> {

  constructor(private articleService: ArticleService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{ res: Article[], total: number}>  {

    let status: CheckStatus;
    if (state.url.indexOf('published') > 0) {
      status = CheckStatus.PASS;
    } else if (state.url.indexOf('unpassed') > 0) {
      status = CheckStatus.UNPASS;
    } else if (state.url.indexOf('undershelf') > 0) {
      status = CheckStatus.UNDERSHELF;
    } else  {
      status = CheckStatus.UNREVIEW;
    }
    return this.articleService.getAllArticles(1, environment.adminPageSize, { status }).pipe(map((res) => {
      if (res.success) {
        return  res;
      } else {
        console.error(res.errorMsg);
      }
    }));
  }
}
